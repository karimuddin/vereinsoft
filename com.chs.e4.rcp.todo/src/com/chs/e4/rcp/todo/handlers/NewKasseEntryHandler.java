package com.chs.e4.rcp.todo.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import com.chs.e4.rcp.todo.parts.KontoSelectionPart;

public class NewKasseEntryHandler {
	@Execute
	public void execute(Shell shell) {
		KontoSelectionPart dialog = new KontoSelectionPart(shell);

		// get the new values from the dialog
		if (dialog.open() == Window.OK) {

		}
	}

}
