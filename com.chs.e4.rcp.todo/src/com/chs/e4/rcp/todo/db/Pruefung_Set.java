package com.chs.e4.rcp.todo.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Pruefung_Set {
	private final MySQLAccess msql;
	private PreparedStatement pstmt1;

	public Pruefung_Set() {
		msql = new MySQLAccess();
		try {
			msql.connectDataBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void set_KassePruefung(int einCent, int zweiCent, int funfCent, int zehnCent, int zwenzigCent,
			int funfzigCent, int einEuro, int zweiEuro, int funfEuro, int zehnEuro, int zwenzigEuro, int funfzigEuro,
			int hundertEuro, int zweihundertEuro, int schickheft, float gesamtBeitrag) {

		try {
			pstmt1 = msql.connect.prepareStatement(
					"UPDATE `verein_software`.`kassepruefung`SET `einCent` = ?, `zweiCent` = ?, `funfCent` = ?, `zehnCent` = ?,`zwenzigCent` = ?,`funfzigCent` = ?,`einEuro` = ?,`zweiEuro` = ?,`funfEuro` = ?,`zehnEuro` = ?,`zwenzigEuro` = ?,`funfzigEuro` = ?,`hundertEuro` = ?,`zweihundertEuro` = ?,`Schickheft` = ?,`gesamtBeitrag` = ? WHERE `idKassepruefung` = 5001;");
			pstmt1.setInt(1, einCent);
			pstmt1.setInt(2, zweiCent);
			pstmt1.setInt(3, funfCent);
			pstmt1.setInt(4, zehnCent);
			pstmt1.setInt(5, zwenzigCent);
			pstmt1.setInt(6, funfzigCent);
			pstmt1.setInt(7, einEuro);
			pstmt1.setInt(8, zweiEuro);
			pstmt1.setInt(9, funfEuro);
			pstmt1.setInt(10, zehnEuro);
			pstmt1.setInt(11, zwenzigEuro);
			pstmt1.setInt(12, funfzigEuro);
			pstmt1.setInt(13, hundertEuro);
			pstmt1.setInt(14, zweihundertEuro);
			pstmt1.setInt(15, schickheft);
			pstmt1.setFloat(16, gesamtBeitrag);
			pstmt1.executeUpdate();
			close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (pstmt1 != null) {
				pstmt1.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
