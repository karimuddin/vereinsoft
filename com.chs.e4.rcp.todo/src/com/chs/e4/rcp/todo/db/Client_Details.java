package com.chs.e4.rcp.todo.db;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class Client_Details {
	MySQLAccess msql;
	public List<String> clientID = new ArrayList<String>();
	public List<String> clientName = new ArrayList<String>();
	public List<String> clientFamilyName = new ArrayList<String>();
	public List<String> clientGender = new ArrayList<String>();
	public List<String> DOB = new ArrayList<String>();
	public List<String> clientAddressID = new ArrayList<String>();
	public List<String> clientStreet = new ArrayList<String>();
	public List<String> clientStreetExtra = new ArrayList<String>();
	public List<String> clientHouseNum = new ArrayList<String>();
	public List<String> clientPostalCode = new ArrayList<String>();
	public List<String> clientCity = new ArrayList<String>();
	public List<String> clientCountry = new ArrayList<String>();
	public List<String> clientState = new ArrayList<String>();
	public List<String> clientTelephone = new ArrayList<String>();
	public String sClientID;
	public int newClientID;

	public Client_Details() {
		msql = new MySQLAccess();
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void get_ClientDetails() {
		try {

			int i = 0;
			msql.connectDataBase();
			msql.resultSet = msql.statement.executeQuery("select * from client_details");

			while (msql.resultSet.next()) {
				sClientID = msql.resultSet.getString(1);
				clientID.add(sClientID);
				clientName.add(msql.resultSet.getString(2));
				clientFamilyName.add(msql.resultSet.getString(3));
				clientGender.add(msql.resultSet.getString(4));
				DOB.add(msql.resultSet.getString(5));

				i++; // counter for getting the number of entries retrieved
			}
			if (sClientID != null) {
				newClientID = Integer.parseInt(sClientID) + 1;
			} else {
				newClientID = 1001;
			}
			System.out.println("Result Set of client_details  has retreived " + i + " columns");
			System.out.println("New Client ID is: " + newClientID);
			msql.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void get_ClientAddress() {
		try {

			int i = 0;
			msql.connectDataBase();
			msql.resultSet = msql.statement.executeQuery("select * from address");

			while (msql.resultSet.next()) {
				clientAddressID.add(msql.resultSet.getString(1));
				clientStreet.add(msql.resultSet.getString(2));
				clientHouseNum.add(msql.resultSet.getString(3));
				clientStreetExtra.add(msql.resultSet.getString(4));
				clientCity.add(msql.resultSet.getString(5));
				clientPostalCode.add(msql.resultSet.getString(6));
				clientCountry.add(msql.resultSet.getString(7));
				clientState.add(msql.resultSet.getString(8));
				clientTelephone.add(msql.resultSet.getString(9));

				i++; // counter for getting the number of entries retrieved
			}

			System.out.println("Result Set of client_address  has retreived " + i + " columns");

			close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (msql.resultSet != null) {
				msql.resultSet.close();
			}

			if (msql.statement != null) {
				msql.statement.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
