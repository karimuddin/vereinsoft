package com.chs.e4.rcp.todo.db;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;

public class Login_Details {
	private final MySQLAccess msql;
	private PreparedStatement pstmt1;
	public List<String> Username = new ArrayList<String>();
	public List<String> Password = new ArrayList<String>();

	public Login_Details() {
		msql = new MySQLAccess();
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public int get_LoginDetails(String username, String password) {

		int i = 0;
		try {
			msql.connectDataBase();
			pstmt1 = msql.connect.prepareStatement(
					"SELECT * FROM verein_software.mitarbeiter Where MitarbeiterBenutzername = ? and MitarbeiterKennwort = ? ;");
			pstmt1.setString(1, username);
			pstmt1.setString(2, password);
			msql.resultSet = pstmt1.executeQuery();
			while (msql.resultSet.next()) {
				Username.add(msql.resultSet.getString(1));
				Password.add(msql.resultSet.getString(2));

				i++; // counter for getting the number of entries retrieved
			}
			System.out.println("Login matched from databse with " + i + " column(s) matched");
			close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return i;

	}

	public void close() {
		try {
			if (msql.resultSet != null) {
				msql.resultSet.close();
			}

			if (msql.statement != null) {
				msql.statement.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
