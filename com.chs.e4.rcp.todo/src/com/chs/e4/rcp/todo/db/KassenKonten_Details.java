package com.chs.e4.rcp.todo.db;

import java.util.ArrayList;
import java.util.List;

public class KassenKonten_Details {
	private final MySQLAccess msql;
	public List<String> idKasseKonten = new ArrayList<String>();
	public List<Float> lebensmittel = new ArrayList<Float>();
	public List<Float> betreuung = new ArrayList<Float>();
	public List<Float> budget = new ArrayList<Float>();
	public List<Float> girokonto = new ArrayList<Float>();
	public List<Float> kasse = new ArrayList<Float>();

	public KassenKonten_Details() {
		msql = new MySQLAccess();
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void get_KassenKontenDetails() {

		int i = 0;
		try {
			msql.connectDataBase();
			msql.resultSet = msql.statement
					.executeQuery("select * from `verein_software`.`kassekonten` WHERE `idKasseKonten` = '7001';");
			while (msql.resultSet.next()) {
				idKasseKonten.add(msql.resultSet.getString(1));
				lebensmittel.add(msql.resultSet.getFloat(2));
				betreuung.add(msql.resultSet.getFloat(3));
				budget.add(msql.resultSet.getFloat(4));
				girokonto.add(msql.resultSet.getFloat(5));
				kasse.add(msql.resultSet.getFloat(6));

				i++; // counter for getting the number of entries retrieved
			}
			System.out.println("Result Set of KasseKonten has retreived " + i + " columns");
			close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (msql.resultSet != null) {
				msql.resultSet.close();
			}

			if (msql.statement != null) {
				msql.statement.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
