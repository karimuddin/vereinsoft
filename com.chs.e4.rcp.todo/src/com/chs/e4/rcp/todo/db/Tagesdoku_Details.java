package com.chs.e4.rcp.todo.db;

import java.util.ArrayList;
import java.util.List;

public class Tagesdoku_Details {
	private final MySQLAccess msql;
	public List<String> idTagesdoku = new ArrayList<String>();
	public List<String> idClient = new ArrayList<String>();
	public List<String> klient = new ArrayList<String>();
	public List<String> betreff = new ArrayList<String>();
	public List<String> kategorie = new ArrayList<String>();
	public List<String> hilfeplan = new ArrayList<String>();
	public List<String> vorfall = new ArrayList<String>();
	public List<String> erfasser = new ArrayList<String>();
	public List<String> eintrag = new ArrayList<String>();

	public Tagesdoku_Details() {
		msql = new MySQLAccess();
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void get_TagesdokuDetails() {
		try {
			int i = 0;
			msql.connectDataBase();
			msql.resultSet = msql.statement.executeQuery("select * from tagesdoku");

			while (msql.resultSet.next()) {
				idTagesdoku.add(msql.resultSet.getString(1));
				idClient.add(msql.resultSet.getString(2));
				klient.add(msql.resultSet.getString(3));
				betreff.add(msql.resultSet.getString(4));
				kategorie.add(msql.resultSet.getString(5));
				hilfeplan.add(msql.resultSet.getString(6));
				vorfall.add(msql.resultSet.getString(7));
				erfasser.add(msql.resultSet.getString(8));
				eintrag.add(msql.resultSet.getString(9));

				i++; // counter for getting the number of entries retrieved
			}
			System.out.println("Result Set of tagesdoku has retreived " + i + " columns");
			close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (msql.resultSet != null) {
				msql.resultSet.close();
			}

			if (msql.statement != null) {
				msql.statement.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
