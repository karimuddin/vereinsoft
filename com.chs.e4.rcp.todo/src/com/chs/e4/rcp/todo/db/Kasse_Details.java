package com.chs.e4.rcp.todo.db;

import java.util.ArrayList;
import java.util.List;

public class Kasse_Details {
	private final MySQLAccess msql;
	public List<String> idKasse = new ArrayList<String>();
	public List<String> idClient = new ArrayList<String>();
	public List<String> clientName = new ArrayList<String>();
	public List<String> account = new ArrayList<String>();
	public List<Float> amount = new ArrayList<Float>();
	public List<String> comment = new ArrayList<String>();
	public List<String> date = new ArrayList<String>();
	public List<String> erfasser = new ArrayList<String>();

	public Kasse_Details() {
		msql = new MySQLAccess();
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void get_KasseDetails() {

		int i = 0;
		try {
			msql.connectDataBase();
			msql.resultSet = msql.statement.executeQuery("select * from kasse");
			while (msql.resultSet.next()) {
				idKasse.add(msql.resultSet.getString(1));
				idClient.add(msql.resultSet.getString(2));
				clientName.add(msql.resultSet.getString(3));
				amount.add(msql.resultSet.getFloat(4));
				account.add(msql.resultSet.getString(5));
				comment.add(msql.resultSet.getString(6));
				date.add(msql.resultSet.getString(7));
				erfasser.add(msql.resultSet.getString(8));

				i++; // counter for getting the number of entries retrieved
			}
			System.out.println("Result Set of Kasse has retreived " + i + " columns");
			close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (msql.resultSet != null) {
				msql.resultSet.close();
			}

			if (msql.statement != null) {
				msql.statement.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
