package com.chs.e4.rcp.todo.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Kasse_Entry {
	private final MySQLAccess msql;
	private PreparedStatement pstmt1;
	private PreparedStatement pstmt2;
	private final Client_Details cDetails;
	private int ClientID;
	public float tempSQl;

	public Kasse_Entry() {
		msql = new MySQLAccess();

		cDetails = new Client_Details();
		cDetails.get_ClientDetails();
		try {
			msql.connectDataBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void set_kasseEntry(int ClientID, String Klient, String Konto, Float Saldo, String Kommentare,
			String Erfasser) {

		System.out.println("New Client ID is recieved: " + ClientID);
		System.out.println(Klient + ": " + Konto + ": " + Saldo + ": " + Kommentare + ": " + Erfasser);

		try {
			pstmt1 = msql.connect.prepareStatement(
					"INSERT INTO `verein_software`.`kasse`(`idClientKasse`,`kasseClientName`,`kasseAmount`,`kasseAccount`,`kasseComment`, KasseErfasser) VALUES ( ?, ?, ?, ?, ?, ?);");
			pstmt1.setInt(1, ClientID);
			pstmt1.setString(2, Klient);
			pstmt1.setFloat(3, Saldo);
			pstmt1.setString(4, Konto);
			pstmt1.setString(5, Kommentare);
			pstmt1.setString(6, Erfasser);
			pstmt1.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (pstmt1 != null) {
				try {
					pstmt1.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}

	/*
	 * public void set_kasseKontenEntry(float lebensmittel, float betreuung,
	 * float budget, float girokonto, float hauptkasse) { try { pstmt2 =
	 * msql.connect.prepareStatement(
	 * "UPDATE `verein_software`.`kassekonten` SET  `Lebensmittel` = ?,`Betreuung` = ?, `Budget` = ?,`Girokonto` = ?,`Kasse` = ? WHERE `idKasseKonten` = '7001';"
	 * ); pstmt2.setFloat(1, lebensmittel); pstmt2.setFloat(2, betreuung);
	 * pstmt2.setFloat(3, budget); pstmt2.setFloat(4, girokonto);
	 * pstmt2.setFloat(5, hauptkasse); pstmt2.executeUpdate();
	 * 
	 * } catch (SQLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } finally {
	 * 
	 * if (pstmt2 != null) { try { pstmt2.close(); } catch (SQLException e) { //
	 * TODO Auto-generated catch block e.printStackTrace(); } }
	 * 
	 * if (msql.connect != null) { try { msql.connect.close(); } catch
	 * (SQLException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } }
	 * 
	 * }
	 * 
	 * }
	 */

	public void set_LebensmittelEntry(float lebensmittel) {
		try {
			msql.resultSet = msql.statement.executeQuery(
					"select `Lebensmittel` from `verein_software`.`kassekonten` WHERE `idKasseKonten` = '7001';");
			while (msql.resultSet.next()) {
				tempSQl = msql.resultSet.getFloat(1);
			}
			pstmt2 = msql.connect.prepareStatement(
					"UPDATE `verein_software`.`kassekonten` SET  `Lebensmittel` = ? WHERE `idKasseKonten` = '7001';");
			pstmt2.setFloat(1, tempSQl + lebensmittel);
			pstmt2.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

	public void set_HauptkasseEntry(float hauptkasse) {
		try {

			msql.resultSet = msql.statement.executeQuery(
					"select `Kasse` from `verein_software`.`kassekonten` WHERE `idKasseKonten` = '7001';");
			while (msql.resultSet.next()) {
				tempSQl = msql.resultSet.getFloat(1);
			}

			pstmt2 = msql.connect.prepareStatement(
					"UPDATE `verein_software`.`kassekonten` SET  `Kasse` = ? WHERE `idKasseKonten` = '7001';");
			pstmt2.setFloat(1, tempSQl + hauptkasse);
			pstmt2.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

	public void set_BetreuungEntry(float betreuung) {
		try {
			msql.resultSet = msql.statement.executeQuery(
					"select `Betreuung` from `verein_software`.`kassekonten` WHERE `idKasseKonten` = '7001';");
			while (msql.resultSet.next()) {
				tempSQl = msql.resultSet.getFloat(1);
			}

			pstmt2 = msql.connect.prepareStatement(
					"UPDATE `verein_software`.`kassekonten` SET  `Betreuung` = ? WHERE `idKasseKonten` = '7001';");

			pstmt2.setFloat(1, tempSQl + betreuung);
			pstmt2.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

	public void set_BudgetEntry(float budget) {
		try {
			msql.resultSet = msql.statement.executeQuery(
					"select `Budget` from `verein_software`.`kassekonten` WHERE `idKasseKonten` = '7001';");
			while (msql.resultSet.next()) {
				tempSQl = msql.resultSet.getFloat(1);
			}
			pstmt2 = msql.connect.prepareStatement(
					"UPDATE `verein_software`.`kassekonten` SET `Budget` = ? WHERE `idKasseKonten` = '7001';");
			pstmt2.setFloat(1, tempSQl + budget);
			pstmt2.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}

	public void set_GirokontoEntry(float girokonto) {
		try {
			msql.resultSet = msql.statement.executeQuery(
					"select `Girokonto` from `verein_software`.`kassekonten` WHERE `idKasseKonten` = '7001';");
			while (msql.resultSet.next()) {
				tempSQl = msql.resultSet.getFloat(1);
			}
			pstmt2 = msql.connect.prepareStatement(
					"UPDATE `verein_software`.`kassekonten` SET `Girokonto` = ? WHERE `idKasseKonten` = '7001';");

			pstmt2.setFloat(1, tempSQl + girokonto);
			pstmt2.executeUpdate();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

			if (pstmt2 != null) {
				try {
					pstmt2.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}

	}
}
