package com.chs.e4.rcp.todo.db;

import java.util.ArrayList;
import java.util.List;

public class Pruefung_Get {
	private final MySQLAccess msql;
	public List<Integer> idKassePruefung = new ArrayList<Integer>();
	public List<Integer> einCent = new ArrayList<Integer>();
	public List<Integer> zweiCent = new ArrayList<Integer>();
	public List<Integer> funfCent = new ArrayList<Integer>();
	public List<Integer> zehnCent = new ArrayList<Integer>();
	public List<Integer> zwenzigCent = new ArrayList<Integer>();
	public List<Integer> funfzigCent = new ArrayList<Integer>();
	public List<Integer> einEuro = new ArrayList<Integer>();
	public List<Integer> zweiEuro = new ArrayList<Integer>();
	public List<Integer> funfEuro = new ArrayList<Integer>();
	public List<Integer> zehnEuro = new ArrayList<Integer>();
	public List<Integer> zwenzigEuro = new ArrayList<Integer>();
	public List<Integer> funfzigEuro = new ArrayList<Integer>();
	public List<Integer> hundertEuro = new ArrayList<Integer>();
	public List<Integer> zweihundertEuro = new ArrayList<Integer>();
	public List<Integer> schickheft = new ArrayList<Integer>();
	public List<Float> gesamtbeitrag = new ArrayList<Float>();

	public Pruefung_Get() {
		msql = new MySQLAccess();
		try {

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void get_KassePruefung() {

		int i = 0;
		try {
			msql.connectDataBase();
			msql.resultSet = msql.statement.executeQuery("SELECT * FROM verein_software.kassepruefung;");
			while (msql.resultSet.next()) {
				idKassePruefung.add(msql.resultSet.getInt(1));
				einCent.add(msql.resultSet.getInt(2));
				zweiCent.add(msql.resultSet.getInt(3));
				funfCent.add(msql.resultSet.getInt(4));
				zehnCent.add(msql.resultSet.getInt(5));
				zwenzigCent.add(msql.resultSet.getInt(6));
				funfzigCent.add(msql.resultSet.getInt(7));
				einEuro.add(msql.resultSet.getInt(8));
				zweiEuro.add(msql.resultSet.getInt(9));
				funfEuro.add(msql.resultSet.getInt(10));
				zehnEuro.add(msql.resultSet.getInt(11));
				zwenzigEuro.add(msql.resultSet.getInt(12));
				funfzigEuro.add(msql.resultSet.getInt(13));
				hundertEuro.add(msql.resultSet.getInt(14));
				zweihundertEuro.add(msql.resultSet.getInt(15));
				schickheft.add(msql.resultSet.getInt(16));
				gesamtbeitrag.add(msql.resultSet.getFloat(17));

				i++; // counter for getting the number of entries retrieved
			}
			System.out.println("Result Set of Kasseprüfung has retreived " + i + " columns");
			close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (msql.resultSet != null) {
				msql.resultSet.close();
			}

			if (msql.statement != null) {
				msql.statement.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
