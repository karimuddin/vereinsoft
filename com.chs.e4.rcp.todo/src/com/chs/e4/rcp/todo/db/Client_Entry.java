package com.chs.e4.rcp.todo.db;

import java.io.FileInputStream;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Client_Entry {
	private final MySQLAccess msql;
	private PreparedStatement pstmt1;
	private PreparedStatement pstmt2;
	private final Client_Details cDetails;
	private int ClientID;

	public Client_Entry() {
		msql = new MySQLAccess();

		cDetails = new Client_Details();
		cDetails.get_ClientDetails();

		try {
			msql.connectDataBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void set_ClientDetails(String cName, String cFamilyName, String cGender, Date dOB, String cFatherName,
			String cMotherName, String cMothertongue, String cConfession, String cNationality, FileInputStream photo) {
		try {

			ClientID = cDetails.newClientID;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("New Client ID is recieved: " + ClientID);

		try {

			System.out.println("Information being fed to SQL : " + ClientID + "   " + cName + "   " + cFamilyName
					+ "   " + cGender + "   " + dOB + "   " + cFatherName + "   " + cMotherName + "   " + cMothertongue
					+ "   " + cConfession + "   " + cNationality);

			pstmt1 = msql.connect.prepareStatement(
					"INSERT INTO `verein_software`.`client_details`(`Client_ID`,`client_name`,`client_family_name`,`client_gender`,`client_dateofbirth`,`client_father_name`,`client_mother_name`,`client_mother_tongue`,`client_confession`,`client_nationality`, `client_image`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);");
			pstmt1.setInt(1, ClientID);
			pstmt1.setString(2, cName);
			pstmt1.setString(3, cFamilyName);
			pstmt1.setString(4, cGender);
			pstmt1.setDate(5, dOB);
			pstmt1.setString(6, cFatherName);
			pstmt1.setString(7, cMotherName);
			pstmt1.setString(8, cMothertongue);
			pstmt1.setString(9, cConfession);
			pstmt1.setString(10, cNationality);
			pstmt1.setBlob(11, photo);
			pstmt1.executeUpdate();

			// close();
		} catch (SQLException e) { // TODO Auto-generated catch block
			System.out.println(e);
		}

	}

	public void set_ClientAddress(String street, String house, String city, String plz, String state,
			String telephone) {

		try {

			pstmt2 = msql.connect.prepareStatement(
					"INSERT INTO `verein_software`.`address`(`idclient_address`,`street`,`house_no`,`city`,`postal_code`,`State`,`Telefone`) VALUES (?,?,?,?,?,?,?);");
			pstmt2.setInt(1, ClientID);
			pstmt2.setString(2, street);
			pstmt2.setString(3, house);
			pstmt2.setString(4, city);
			pstmt2.setString(5, plz);
			pstmt2.setString(6, state);
			pstmt2.setString(7, telephone);
			pstmt2.executeUpdate();

		} catch (SQLException e) { // TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (pstmt1 != null) {
				pstmt1.close();
			}
			if (pstmt2 != null) {
				pstmt2.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
