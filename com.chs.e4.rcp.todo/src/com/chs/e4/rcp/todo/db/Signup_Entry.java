package com.chs.e4.rcp.todo.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Signup_Entry {
	private final MySQLAccess msql;
	private PreparedStatement pstmt1;

	public Signup_Entry() {
		msql = new MySQLAccess();

		try {
			msql.connectDataBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public int set_SignupEntry(String EmpName, String EmpFamilienName, String EmpBenutzerName, String kennwort) {
		int i = 0;
		try {
			pstmt1 = msql.connect.prepareStatement(
					"INSERT INTO `verein_software`.`mitarbeiter`(`MitarbeiterName`,`MitarbeiterFamilyname`,`MitarbeiterBenutzername`,`MitarbeiterKennwort`) VALUES ( ?, ?, ?, ?);");

			pstmt1.setString(1, EmpName);
			pstmt1.setString(2, EmpFamilienName);
			pstmt1.setString(3, EmpBenutzerName);
			pstmt1.setString(4, kennwort);
			i = pstmt1.executeUpdate();
			System.out.println("Emplyee has been created " + i);
			close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e);

		}
		return i;

	}

	public void close() {
		try {
			if (pstmt1 != null) {
				pstmt1.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}

}
