package com.chs.e4.rcp.todo.db;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Tagesdoku_Entry {
	private final MySQLAccess msql;
	private PreparedStatement pstmt1;
	private PreparedStatement pstmt2;
	private final Client_Details cDetails;
	private int ClientID;

	public Tagesdoku_Entry() {
		msql = new MySQLAccess();

		cDetails = new Client_Details();
		cDetails.get_ClientDetails();
		try {
			msql.connectDataBase();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void set_tagesdokuEntry(int ClientID, String Klient, String Betreff, String Kategorie, String Hilfeplan,
			String Erfasser, String Eintrag) {

		System.out.println("New Client ID is recieved: " + ClientID);
		System.out.println(
				Klient + ": " + Betreff + ": " + Kategorie + ": " + Hilfeplan + ": " + Erfasser + ": " + Eintrag);

		try {
			pstmt1 = msql.connect.prepareStatement(
					"INSERT INTO `verein_software`.`tagesdoku`(`idClient`,`Klient`,`Betreff`,`Kategorie`,`Hilfeplan`,`Erfasser`,`Eintrag`) VALUES (?, ?, ?, ?, ?, ?, ?);");
			pstmt1.setInt(1, ClientID);
			pstmt1.setString(2, Klient);
			pstmt1.setString(3, Betreff);
			pstmt1.setString(4, Kategorie);
			pstmt1.setString(5, Hilfeplan);
			pstmt1.setString(6, Erfasser);
			pstmt1.setString(7, Eintrag);
			pstmt1.executeUpdate();
			close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void close() {
		try {
			if (pstmt1 != null) {
				pstmt1.close();
			}

			if (msql.connect != null) {
				msql.connect.close();
			}
		} catch (Exception e) {

		}
	}
}
