package com.chs.e4.rcp.todo;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.chs.e4.rcp.todo.db.Signup_Entry;

public class SignupDialog extends TitleAreaDialog {
	private Text textname;
	private Text textfamilyname;
	private Text textUsername;
	private Text textPassword;

	@Inject
	MApplication application;

	public SignupDialog(Shell parentShell) {
		super(parentShell);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);
		setTitle("Registration");
		setMessage("Bitte registrieren Sie mit allen Details");
		return contents;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		System.out.println(application);

		Composite container = new Composite(area, SWT.NULL);
		container.setLayout(new GridLayout(2, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		Label lblName = new Label(container, SWT.NONE);
		lblName.setText("Name");
		textname = new Text(container, SWT.BORDER);
		textname.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label lblFamilienname = new Label(container, SWT.NONE);
		lblFamilienname.setText("Familienname");
		textfamilyname = new Text(container, SWT.BORDER);
		textfamilyname.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label label_1 = new Label(container, SWT.NULL);
		label_1.setText("Benutzername");
		textUsername = new Text(container, SWT.BORDER);
		textUsername.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Label label = new Label(container, SWT.NULL);
		label.setText("Kennwort");
		textPassword = new Text(container, SWT.PASSWORD | SWT.BORDER);
		textPassword.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		return area;
	}

	@Override
	protected Point getInitialSize() {
		return new Point(580, 316);
	}

	@Override
	protected void okPressed() {
		// Defining Tagesdoku_entry class
		Signup_Entry se = new Signup_Entry();

		// passing values of the complete form

		int result = se.set_SignupEntry(textname.getText(), textfamilyname.getText(), textUsername.getText(),
				textPassword.getText());
		if (result > 0) {
			super.okPressed();
		} else {
			MessageDialog.openError(new Shell(), "Fehler",
					"Bitte geben sie vollstandinge information oder wenn alle informationen gegeben, bitte wähles sie andere benutzername");
		}

	}

	@Override
	protected void cancelPressed() {
		System.exit(-1);
	}

}
