package com.chs.e4.rcp.todo.parts;

import javax.annotation.PostConstruct;

import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;

import com.chs.e4.rcp.todo.db.Tagesdoku_Details;

public class DetailsTagesdokuPart {
	private Table table;

	@PostConstruct
	public void createControls(Composite parent, EMenuService menuService) {

		Tagesdoku_Details td = new Tagesdoku_Details();
		td.get_TagesdokuDetails();

		java.util.List<String> dokuID = td.idTagesdoku;
		java.util.List<String> vorfall = td.vorfall;
		java.util.List<String> klient = td.klient;
		java.util.List<String> betreff = td.betreff;
		java.util.List<String> kategorie = td.kategorie;
		java.util.List<String> hilfeplan = td.hilfeplan;
		java.util.List<String> eintrag = td.eintrag;
		java.util.List<String> erfasser = td.erfasser;

		parent.setLayout(new GridLayout(2, false));

		table = new Table(parent, SWT.BORDER | SWT.FULL_SELECTION);
		GridData gd_table = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 3);
		gd_table.heightHint = 483;
		gd_table.widthHint = 754;
		table.setLayoutData(gd_table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);

		TableColumn tblclmnVorfallAm = new TableColumn(table, SWT.NONE);
		tblclmnVorfallAm.setWidth(107);
		tblclmnVorfallAm.setText("Vorfall am");

		TableColumn tblclmnKlientgruppe = new TableColumn(table, SWT.NONE);
		tblclmnKlientgruppe.setWidth(100);
		tblclmnKlientgruppe.setText("Klient/Gruppe");

		TableColumn tblclmnBetreff = new TableColumn(table, SWT.NONE);
		tblclmnBetreff.setWidth(100);
		tblclmnBetreff.setText("Betreff");

		TableColumn tblclmnKategorie = new TableColumn(table, SWT.NONE);
		tblclmnKategorie.setWidth(100);
		tblclmnKategorie.setText("Kategorie");

		TableColumn tblclmnHilfeplan = new TableColumn(table, SWT.NONE);
		tblclmnHilfeplan.setWidth(100);
		tblclmnHilfeplan.setText("Hilfeplan");

		TableColumn tblclmnEintrag = new TableColumn(table, SWT.NONE);
		tblclmnEintrag.setWidth(283);
		tblclmnEintrag.setText("Eintrag");

		// Populating each row with data
		for (int i = 0; i < dokuID.size(); i++) {
			TableItem item = new TableItem(table, SWT.NONE);
			item.setText(0, vorfall.get(i));
			item.setText(1, klient.get(i));
			item.setText(2, betreff.get(i));
			item.setText(3, kategorie.get(i));
			item.setText(4, hilfeplan.get(i));
			item.setText(5, eintrag.get(i));

			/*
			 * System.out.println( ID.get(i) + " " + name.get(i) + " " +
			 * family_name.get(i) + " " + gender.get(i) + " " + dob.get(i));
			 */
		}

		tblclmnVorfallAm.pack();
		tblclmnKlientgruppe.pack();
		tblclmnBetreff.pack();
		tblclmnKategorie.pack();
		tblclmnHilfeplan.pack();
		tblclmnEintrag.pack();

		Group grpVorgang = new Group(parent, SWT.NONE);
		grpVorgang.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpVorgang.setLayout(new GridLayout(2, false));
		GridData gd_grpVorgang = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_grpVorgang.heightHint = 100;
		gd_grpVorgang.widthHint = 269;
		grpVorgang.setLayoutData(gd_grpVorgang);
		grpVorgang.setText("Vorgang");

		Label lblDokuart = new Label(grpVorgang, SWT.NONE);
		lblDokuart.setText("DokuArt");

		Label lblDokuartdynamic = new Label(grpVorgang, SWT.NONE);
		lblDokuartdynamic.setText("Bericht");

		Label lblKategorie = new Label(grpVorgang, SWT.NONE);
		lblKategorie.setText("Kategorie");

		Label lblKategoriedynamic = new Label(grpVorgang, SWT.NONE);
		GridData gd_lblKategoriedynamic = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblKategoriedynamic.widthHint = 181;
		lblKategoriedynamic.setLayoutData(gd_lblKategoriedynamic);
		lblKategoriedynamic.setText(kategorie.get(0));

		Label lblHilfeplan = new Label(grpVorgang, SWT.NONE);
		lblHilfeplan.setText("Hilfeplan");

		Label lblHilfeplandynamic = new Label(grpVorgang, SWT.NONE);
		GridData gd_lblHilfeplandynamic = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblHilfeplandynamic.widthHint = 181;
		lblHilfeplandynamic.setLayoutData(gd_lblHilfeplandynamic);
		lblHilfeplandynamic.setText(hilfeplan.get(0) + " ");

		Label lblVorgang = new Label(grpVorgang, SWT.NONE);
		lblVorgang.setText("Vorfall ");

		Label lblVorfalldynamic = new Label(grpVorgang, SWT.NONE);
		GridData gd_lblVorfalldynamic = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblVorfalldynamic.widthHint = 180;
		lblVorfalldynamic.setLayoutData(gd_lblVorfalldynamic);
		lblVorfalldynamic.setText(vorfall.get(0) + " ");

		Label lblErfasser = new Label(grpVorgang, SWT.NONE);
		lblErfasser.setText("Erfasser");

		Label lblErfasserdynamic = new Label(grpVorgang, SWT.NONE);
		GridData gd_lblErfasserdynamic = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblErfasserdynamic.widthHint = 179;
		lblErfasserdynamic.setLayoutData(gd_lblErfasserdynamic);
		lblErfasserdynamic.setText(erfasser.get(0) + " ");

		Group grpTeilnehmer = new Group(parent, SWT.NONE);
		grpTeilnehmer.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpTeilnehmer.setLayout(new GridLayout(1, false));
		GridData gd_grpTeilnehmer = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_grpTeilnehmer.heightHint = 35;
		gd_grpTeilnehmer.widthHint = 270;
		grpTeilnehmer.setLayoutData(gd_grpTeilnehmer);
		grpTeilnehmer.setText("Teilnehmer");

		Label lblTeilnehmerklient = new Label(grpTeilnehmer, SWT.NONE);
		GridData gd_lblTeilnehmerklient = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblTeilnehmerklient.heightHint = 25;
		gd_lblTeilnehmerklient.widthHint = 259;
		lblTeilnehmerklient.setLayoutData(gd_lblTeilnehmerklient);
		lblTeilnehmerklient.setText(klient.get(0) + " ");

		Group grpDokumentation = new Group(parent, SWT.NONE);
		grpDokumentation.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		grpDokumentation.setLayout(new GridLayout(1, false));
		GridData gd_grpDokumentation = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_grpDokumentation.widthHint = 269;
		gd_grpDokumentation.heightHint = 281;
		grpDokumentation.setLayoutData(gd_grpDokumentation);
		grpDokumentation.setText("Dokumentation");

		Label lblBetreff = new Label(grpDokumentation, SWT.NONE);
		GridData gd_lblBetreff = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblBetreff.widthHint = 213;
		lblBetreff.setLayoutData(gd_lblBetreff);
		lblBetreff.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblBetreff.setText(betreff.get(0));
		new Label(grpDokumentation, SWT.NONE);

		Label lblEintrag = new Label(grpDokumentation, SWT.NONE);
		GridData gd_lblEintrag = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblEintrag.widthHint = 48;
		lblEintrag.setLayoutData(gd_lblEintrag);
		lblEintrag.setFont(SWTResourceManager.getFont("Segoe UI", 9, SWT.BOLD));
		lblEintrag.setText("Eintrag");

		Label lblEintragdynamic = new Label(grpDokumentation, SWT.WRAP);
		GridData gd_lblEintragdynamic = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblEintragdynamic.widthHint = 214;
		gd_lblEintragdynamic.heightHint = 132;
		lblEintragdynamic.setLayoutData(gd_lblEintragdynamic);
		lblEintragdynamic.setText(eintrag.get(0));

		table.setSize(table.computeSize(SWT.DEFAULT, SWT.DEFAULT));
		table.addListener(SWT.Selection, new Listener() {
			public void handleEvent(Event e) {
				String string = "";
				TableItem[] selection = table.getSelection();

				int i;
				for (i = 0; i < selection.length; i++)
					string = selection[i] + " ";

				lblKategoriedynamic.setText(kategorie.get(table.getSelectionIndex()));
				lblHilfeplandynamic.setText(hilfeplan.get(table.getSelectionIndex()));
				lblVorfalldynamic.setText(vorfall.get(table.getSelectionIndex()));
				lblErfasserdynamic.setText(erfasser.get(table.getSelectionIndex()));
				lblTeilnehmerklient.setText(klient.get(table.getSelectionIndex()));
				lblBetreff.setText(betreff.get(table.getSelectionIndex()));
				lblEintragdynamic.setText(eintrag.get(table.getSelectionIndex()));

				System.out.println("DefaultSelection={" + string + "}");
			}
		});

	}
}
