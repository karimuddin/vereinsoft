package com.chs.e4.rcp.todo.parts;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.chs.e4.rcp.todo.db.Client_Details;
import com.chs.e4.rcp.todo.db.Kasse_Entry;

public class KasseEntryPart extends Dialog {
	private String Client[];
	private Text txtKomment;
	private Combo comboKlient;
	private Combo comboKonto;
	private Combo comboGegenkonto;
	private Spinner spnrSaldo;
	private Label lblErfasserName;

	private static String erfasser;
	List<String> ID;

	@Inject
	public KasseEntryPart(Shell shell) {
		super(shell);

		// TODO Auto-generated constructor stub
	}

	// private Text text;

	/*
	 * @PostConstruct public void createControls(Composite parent, EMenuService
	 * menuService) { parent.setLayout(new GridLayout(2, false));
	 * 
	 * Label lblNewLabel = new Label(parent, SWT.NONE);
	 * lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
	 * false, 1, 1)); lblNewLabel.setText("New Label");
	 * 
	 * text = new Text(parent, SWT.BORDER); text.setLayoutData(new
	 * GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	 * 
	 * }
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Client_Details cDetails = new Client_Details();
		cDetails.get_ClientDetails();
		ID = cDetails.clientID;
		List<String> name = cDetails.clientName;
		List<String> family_name = cDetails.clientFamilyName;

		try {
			Client = new String[ID.size()];
			for (int i = 0; i < ID.size(); i++) {

				Client[i] = name.get(i) + ", " + family_name.get(i);

				System.out.println("whats UP " + Client[i]);
			}

		} catch (

		Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(2, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);
		new Label(container, SWT.NONE);

		Label lblKinder = new Label(container, SWT.NONE);
		lblKinder.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		lblKinder.setText("Kinder Kasse");
		lblKinder.setFont(SWTResourceManager.getFont("Segoe UI Light", 11, SWT.BOLD));

		Label lblKind = new Label(container, SWT.NONE);
		GridData gd_lblKind = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_lblKind.widthHint = 71;
		lblKind.setLayoutData(gd_lblKind);
		lblKind.setText("Kind");

		comboKlient = new Combo(container, SWT.NONE);
		comboKlient.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		comboKlient.setItems(Client);
		comboKlient.select(0);

		Label lblKonto = new Label(container, SWT.NONE);
		GridData gd_lblKonto = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_lblKonto.widthHint = 72;
		lblKonto.setLayoutData(gd_lblKonto);
		lblKonto.setText("Konto");

		comboKonto = new Combo(container, SWT.NONE);
		comboKonto.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		String[] kategorien = { "Taschengeld", "Bekleidungsgeld", "Lebensmittel" };
		comboKonto.setItems(kategorien);
		comboKonto.select(0);

		Label lblGegenKonto = new Label(container, SWT.NONE);
		GridData gd_lblGegenKonto = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_lblGegenKonto.widthHint = 72;
		lblGegenKonto.setLayoutData(gd_lblGegenKonto);
		lblGegenKonto.setText("Gegenkonto");

		comboGegenkonto = new Combo(container, SWT.NONE);
		comboGegenkonto.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		String[] kgs = { "Hauptkasse", "Girokonto", "Betreuung", "Budget", "Lebensmittel" };
		comboGegenkonto.setItems(kgs);
		comboGegenkonto.select(0);

		Label lblSaldo = new Label(container, SWT.NONE);
		GridData gd_lblSaldo = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblSaldo.widthHint = 40;
		lblSaldo.setLayoutData(gd_lblSaldo);
		lblSaldo.setText("Saldo");

		spnrSaldo = new Spinner(container, SWT.BORDER);
		spnrSaldo.setDigits(2);
		spnrSaldo.setMaximum(100000000);

		Label lblKommentare = new Label(container, SWT.NONE);
		lblKommentare.setText("Kommetare");

		txtKomment = new Text(container, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_txtKomment = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtKomment.heightHint = 34;
		txtKomment.setLayoutData(gd_txtKomment);
		txtKomment.setTextLimit(200);

		Label lblErfasser = new Label(container, SWT.NONE);
		lblErfasser.setText("Erfasser");

		lblErfasserName = new Label(container, SWT.NONE);
		lblErfasserName.setText(get_Erfasser() + "");

		return container;
	}

	// override method to use "Login" as label for the OK button
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Submit", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(767, 292);
	}

	@Override
	protected void okPressed() {
		float saldo = spnrSaldo.getSelection() / (float) 100;

		System.out.println("Values to new kasse enrty: " + ID.get(comboKlient.getSelectionIndex()) + " "
				+ comboKlient.getText() + " " + comboKonto.getText() + " " + saldo + " " + txtKomment.getText() + " "
				+ lblErfasserName.getText() + "");

		// Defining Tagesdoku_entry class

		Kasse_Entry ke = new Kasse_Entry();

		// passing values of the complete form

		String gegenkonto = comboGegenkonto.getItem(comboGegenkonto.getSelectionIndex());

		if (gegenkonto.equals("Hauptkasse")) {

			ke.set_HauptkasseEntry(-saldo);

			ke.set_kasseEntry(Integer.parseInt(ID.get(comboKlient.getSelectionIndex())), comboKlient.getText(),
					comboKonto.getText() + "/" + comboGegenkonto.getText(), -saldo, txtKomment.getText() + "",
					lblErfasserName.getText() + "");

		} else if (gegenkonto.equals("Girokonto")) {

			ke.set_GirokontoEntry(-saldo);

			ke.set_kasseEntry(Integer.parseInt(ID.get(comboKlient.getSelectionIndex())), comboKlient.getText(),
					comboKonto.getText() + "/" + comboGegenkonto.getText(), -saldo, txtKomment.getText() + "",
					lblErfasserName.getText() + "");
		} else if (gegenkonto.equals("Betreuung")) {

			ke.set_BetreuungEntry(-saldo);

			ke.set_kasseEntry(Integer.parseInt(ID.get(comboKlient.getSelectionIndex())), comboKlient.getText(),
					comboKonto.getText() + "/" + comboGegenkonto.getText(), -saldo, txtKomment.getText() + "",
					lblErfasserName.getText() + "");
		} else if (gegenkonto.equals("Budget")) {

			ke.set_BudgetEntry(-saldo);

			ke.set_kasseEntry(Integer.parseInt(ID.get(comboKlient.getSelectionIndex())), comboKlient.getText(),
					comboKonto.getText() + "/" + comboGegenkonto.getText(), -saldo, txtKomment.getText() + "",
					lblErfasserName.getText() + "");
		} else if (gegenkonto.equals("Lebensmittel")) {

			ke.set_LebensmittelEntry(-saldo);

			ke.set_kasseEntry(Integer.parseInt(ID.get(comboKlient.getSelectionIndex())), comboKlient.getText(),
					comboKonto.getText() + "/" + comboGegenkonto.getText(), -saldo, txtKomment.getText() + "",
					lblErfasserName.getText() + "");
		}

		super.okPressed();

	}

	public void set_Erfasser(String erf) {
		erfasser = erf;

	}

	public String get_Erfasser() {
		return erfasser;
	}

}
