package com.chs.e4.rcp.todo.parts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Date;
import java.util.Calendar;

import javax.annotation.PostConstruct;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CCombo;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.events.PaintEvent;
import org.eclipse.swt.events.PaintListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Text;

import com.chs.e4.rcp.todo.db.Client_Entry;

public class TodoOverviewPart {
	private static Text geburtsort;
	private static Text plz;
	private static Text ort;
	private static Text bundesland;
	private static Text street;
	private static Text telefon;
	private static Text vorName;
	private GridData gridData_1;
	private Table table;
	private static Date dob;
	private static Text name;
	private static CCombo geschlecht;
	private Text house;

	private static Canvas canvas;
	private static GridData gd_canvas;
	private static Image photo;
	private FileInputStream fis;

	private Client_Entry clientEntry;
	private static Text txtVater;
	private static Text txtMutter;
	private static Text txtMuttersprache;
	private static Text txtNationalitt;
	private static Text txtKonfession;

	public TodoOverviewPart() {
		clientEntry = new Client_Entry();
	}

	@PostConstruct
	public void createControls(Composite parent) {

		/*
		 * System.out.println(this.getClass().getSimpleName() +
		 * " @PostConstruct method called.");
		 */

		/*
		 * FillLayout layout = new FillLayout(SWT.VERTICAL);
		 * parent.setLayout(layout); for (int i = 0; i < 8; ++i) { Button button
		 * = new Button(parent, SWT.NONE); button.setText("Sample Text"); }
		 */

		GridLayout gridlayout = new GridLayout();
		parent.setLayout(gridlayout);

		GridData gridData = new GridData(GridData.FILL, GridData.CENTER, true, false);
		gridData.horizontalSpan = 2;

		// ---------------Large Group1
		Group kindInfo = new Group(parent, SWT.NONE);
		kindInfo.setLayout(new GridLayout(2, false));
		gridData_1 = new GridData(GridData.FILL, GridData.CENTER, true, false);
		gridData_1.widthHint = 754;
		gridData_1.horizontalSpan = 1;
		kindInfo.setLayoutData(gridData_1);

		// ----------Child Photo -----------------
		canvas = new Canvas(kindInfo, SWT.NONE);
		gd_canvas = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_canvas.heightHint = 111;
		gd_canvas.widthHint = 124;
		canvas.setLayoutData(gd_canvas);
		canvas.addPaintListener(new PaintListener() {
			public void paintControl(final PaintEvent event) {
				if (photo != null) {
					event.gc.setAntialias(SWT.ON);
					event.gc.setInterpolation(SWT.HIGH);
					event.gc.drawImage(photo, 0, 0, photo.getBounds().width, photo.getBounds().height, 0, 0,
							gd_canvas.widthHint, gd_canvas.heightHint);
				}
			}
		});

		SashForm sashForm = new SashForm(kindInfo, SWT.NONE);
		GridData gd_sashForm = new GridData(SWT.FILL, SWT.FILL, false, false, 1, 1);
		gd_sashForm.widthHint = 804;
		sashForm.setLayoutData(gd_sashForm);

		// ----------Child -----------------
		Group kind = new Group(sashForm, SWT.NONE);
		kind.setText("Kind");
		gridlayout = new GridLayout();
		gridlayout.numColumns = 2;
		kind.setLayout(gridlayout);

		new Label(kind, SWT.NONE).setText("Vorname");
		vorName = new Text(kind, SWT.SINGLE | SWT.BORDER);
		vorName.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(kind, SWT.NONE).setText("Name");
		name = new Text(kind, SWT.SINGLE | SWT.BORDER);
		name.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false));

		new Label(kind, SWT.NONE).setText("Geschlecht");
		geschlecht = new CCombo(kind, SWT.BORDER);
		GridData gd_geschlecht = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_geschlecht.widthHint = 203;
		geschlecht.setLayoutData(gd_geschlecht);
		geschlecht.add("männlich");
		geschlecht.add("weiblich");
		geschlecht.select(0);

		Label label = new Label(kind, SWT.NONE);
		label.setText("Geburtsdatum");

		DateTime geburtsdatum = new DateTime(kind, SWT.BORDER);
		dob = new Date(geburtsdatum.getYear() - 1900, geburtsdatum.getMonth(), geburtsdatum.getDay());
		geburtsdatum.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("calendar date changed");
				dob = new Date(geburtsdatum.getYear() - 1900, geburtsdatum.getMonth(), geburtsdatum.getDay());
				System.out.print("Date of Birth is " + dob);
			}

		});
		GridData gd_geburtsdatum = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_geburtsdatum.widthHint = 208;
		geburtsdatum.setLayoutData(gd_geburtsdatum);

		new Label(kind, SWT.NONE).setText("Geb.-ort");

		geburtsort = new Text(kind, SWT.BORDER);
		GridData gd_geburtsort = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_geburtsort.widthHint = 196;
		geburtsort.setLayoutData(gd_geburtsort);

		// ----------Child Address -----------------
		Group grpAddresseDesKindes = new Group(sashForm, SWT.NONE);
		grpAddresseDesKindes.setLayout(new GridLayout(3, false));
		grpAddresseDesKindes.setText("Addresse des Kindes");

		Label lblNewLabel = new Label(grpAddresseDesKindes, SWT.NONE);
		lblNewLabel.setText("Straße Nr:");

		street = new Text(grpAddresseDesKindes, SWT.BORDER);
		GridData gd_street = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_street.widthHint = 182;
		street.setLayoutData(gd_street);

		house = new Text(grpAddresseDesKindes, SWT.BORDER);
		GridData gd_house = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_house.widthHint = 77;
		house.setLayoutData(gd_house);

		Label lblNewLabel_1 = new Label(grpAddresseDesKindes, SWT.NONE);
		lblNewLabel_1.setText("PLZ");

		plz = new Text(grpAddresseDesKindes, SWT.BORDER);
		plz.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(grpAddresseDesKindes, SWT.NONE);

		Label lblNewLabel_2 = new Label(grpAddresseDesKindes, SWT.NONE);
		lblNewLabel_2.setText("Ort");

		ort = new Text(grpAddresseDesKindes, SWT.BORDER);
		ort.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(grpAddresseDesKindes, SWT.NONE);

		Label lblBundesland = new Label(grpAddresseDesKindes, SWT.NONE);
		lblBundesland.setText("Bundesland");

		bundesland = new Text(grpAddresseDesKindes, SWT.BORDER);
		bundesland.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(grpAddresseDesKindes, SWT.NONE);

		Label lblTelefon = new Label(grpAddresseDesKindes, SWT.NONE);
		lblTelefon.setText("Telefon");

		telefon = new Text(grpAddresseDesKindes, SWT.BORDER);
		telefon.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(grpAddresseDesKindes, SWT.NONE);
		sashForm.setWeights(new int[] { 1, 1 });

		Group grpDetailsZumKind = new Group(parent, SWT.NONE);
		grpDetailsZumKind.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		grpDetailsZumKind.setText("Details zum Kind");
		grpDetailsZumKind.setLayout(new GridLayout(4, false));

		Label lblKonfession = new Label(grpDetailsZumKind, SWT.NONE);
		lblKonfession.setText("Konfession");

		txtKonfession = new Text(grpDetailsZumKind, SWT.BORDER);
		txtKonfession.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblVater = new Label(grpDetailsZumKind, SWT.NONE);
		lblVater.setText("Vater");

		txtVater = new Text(grpDetailsZumKind, SWT.BORDER);
		txtVater.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblNationalitt = new Label(grpDetailsZumKind, SWT.NONE);
		lblNationalitt.setText("Nationalität");

		txtNationalitt = new Text(grpDetailsZumKind, SWT.BORDER);
		txtNationalitt.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblMutter = new Label(grpDetailsZumKind, SWT.NONE);
		lblMutter.setText("Mutter");

		txtMutter = new Text(grpDetailsZumKind, SWT.BORDER);
		txtMutter.setText("");
		txtMutter.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblMuttersprache = new Label(grpDetailsZumKind, SWT.NONE);
		lblMuttersprache.setText("Muttersprache");

		txtMuttersprache = new Text(grpDetailsZumKind, SWT.BORDER);
		txtMuttersprache.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		new Label(grpDetailsZumKind, SWT.NONE);
		new Label(grpDetailsZumKind, SWT.NONE);

		/*
		 * // --------------------Group Betreuungzeiten--------------------
		 * Group grpBetreuungszeiten = new Group(parent, SWT.NONE);
		 * grpBetreuungszeiten.setLayout(new FormLayout()); GridData
		 * gd_grpBetreuungszeiten = new GridData(SWT.FILL, SWT.CENTER, false,
		 * false, 1, 1); gd_grpBetreuungszeiten.widthHint = 768;
		 * grpBetreuungszeiten.setLayoutData(gd_grpBetreuungszeiten);
		 * grpBetreuungszeiten.setText("Betreuungszeiten");
		 * 
		 * table = new Table(grpBetreuungszeiten, SWT.BORDER |
		 * SWT.FULL_SELECTION | SWT.HIDE_SELECTION); FormData fd_table = new
		 * FormData(); fd_table.bottom = new FormAttachment(95); fd_table.right
		 * = new FormAttachment(100, -39); fd_table.top = new FormAttachment(0,
		 * 25); fd_table.left = new FormAttachment(0, 13);
		 * table.setLayoutData(fd_table); table.setHeaderVisible(true);
		 * table.setLinesVisible(true);
		 * 
		 * TableColumn tblclmnZeitraum = new TableColumn(table, SWT.NONE);
		 * tblclmnZeitraum.setWidth(125); tblclmnZeitraum.setText("Zeitraum");
		 * 
		 * TableColumn tblclmnMontag = new TableColumn(table, SWT.NONE);
		 * tblclmnMontag.setWidth(100); tblclmnMontag.setText("Montag");
		 * 
		 * TableColumn tblclmnDienstag = new TableColumn(table, SWT.NONE);
		 * tblclmnDienstag.setWidth(100); tblclmnDienstag.setText("Dienstag");
		 * 
		 * TableColumn tblclmnMittwoch = new TableColumn(table, SWT.NONE);
		 * tblclmnMittwoch.setWidth(100); tblclmnMittwoch.setText("Mittwoch");
		 * 
		 * TableColumn tblclmnDonnerstag = new TableColumn(table, SWT.NONE);
		 * tblclmnDonnerstag.setWidth(100);
		 * tblclmnDonnerstag.setText("Donnerstag");
		 * 
		 * TableColumn tblclmnFreitag = new TableColumn(table, SWT.NONE);
		 * tblclmnFreitag.setWidth(77); tblclmnFreitag.setText("Freitag");
		 * 
		 * TableColumn tblclmnVerflegungstage = new TableColumn(table,
		 * SWT.NONE); tblclmnVerflegungstage.setWidth(138);
		 * tblclmnVerflegungstage.setText("Verflegungstage");
		 * 
		 * TableColumn tblclmnDurschBetr = new TableColumn(table, SWT.NONE);
		 * tblclmnDurschBetr.setWidth(100); tblclmnDurschBetr.setText(
		 * "Dursch. Betr.");
		 * 
		 * for (int i = 0; i < 2; i++) { TableItem item = new TableItem(table,
		 * SWT.NONE); item.setText(new String[] { " " + i, "A ", "B ", "C ",
		 * "D ", "E ", "F ", "G " }); }
		 * 
		 * // --------------Code for making table editable----------- final
		 * TableEditor editor = new TableEditor(table);
		 * editor.horizontalAlignment = SWT.LEFT; editor.grabHorizontal = true;
		 * table.addListener(SWT.MouseDown, event -> { Rectangle clientArea =
		 * table.getClientArea(); Point pt = new Point(event.x, event.y); int
		 * index = table.getTopIndex(); while (index < table.getItemCount()) {
		 * boolean visible = false; final TableItem item = table.getItem(index);
		 * for (int i = 0; i < table.getColumnCount(); i++) { Rectangle rect =
		 * item.getBounds(i); if (rect.contains(pt)) { final int column = i;
		 * final Text text = new Text(table, SWT.NONE); Listener textListener =
		 * e -> { switch (e.type) { case SWT.FocusOut: item.setText(column,
		 * text.getText()); text.dispose(); break; case SWT.Traverse: switch
		 * (e.detail) { case SWT.TRAVERSE_RETURN: item.setText(column,
		 * text.getText()); // FALL THROUGH case SWT.TRAVERSE_ESCAPE:
		 * text.dispose(); e.doit = false; } break; } };
		 * text.addListener(SWT.FocusOut, textListener);
		 * text.addListener(SWT.Traverse, textListener); editor.setEditor(text,
		 * item, i); text.setText(item.getText(i)); text.selectAll();
		 * text.setFocus(); return; } if (!visible &&
		 * rect.intersects(clientArea)) { visible = true; } } if (!visible)
		 * return; index++; } }); System.out.println("Zeitraum : " +
		 * table.getItem(0).getText(1));
		 * 
		 * tblclmnZeitraum.pack(); tblclmnMontag.pack(); tblclmnDienstag.pack();
		 * tblclmnMittwoch.pack(); tblclmnDonnerstag.pack();
		 * tblclmnFreitag.pack(); tblclmnVerflegungstage.pack();
		 * tblclmnDurschBetr.pack();
		 */
		Button btnEnter = new Button(parent, SWT.NONE);
		GridData gd_btnEnter = new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1);
		gd_btnEnter.widthHint = 61;
		btnEnter.setLayoutData(gd_btnEnter);
		btnEnter.setText("Enter");
		btnEnter.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {

				try {
					sendData();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				clearData();

				Calendar startCal = Calendar.getInstance();
				geburtsdatum.setDate(startCal.get(Calendar.YEAR), startCal.get(Calendar.MONTH),
						startCal.get(Calendar.DAY_OF_MONTH));

			}

		});

		Button btnBrowsePhoto = new Button(parent, SWT.NONE);
		btnBrowsePhoto.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		btnBrowsePhoto.setText("Profilbild Hochladen");
		btnBrowsePhoto.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				String fileName = new FileDialog(parent.getShell()).open();
				try {
					fis = new FileInputStream(fileName);
					System.out.println(fis + " ");
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (fileName != null) {
					photo = new Image(parent.getDisplay(), fileName);
					canvas.redraw();
				} else {
					MessageDialog msgDialog = new MessageDialog(parent.getShell(), "Error", null,
							"Please an Image of the Child", MessageDialog.ERROR, new String[] { "OK" }, 0);
					msgDialog.open();

				}
			}

		});

	}

	protected void sendData() {

		clientEntry.set_ClientDetails(vorName.getText() + "", name.getText() + "", geschlecht.getText() + "", dob,
				txtVater.getText() + "", txtMutter.getText() + "", txtMuttersprache.getText() + "",
				txtKonfession.getText() + "", txtNationalitt.getText() + "", fis);
		clientEntry.set_ClientAddress(street.getText() + "", house.getText() + "", ort.getText() + "",
				plz.getText() + "", bundesland.getText() + "", telefon.getText() + "");
		System.out.println("Information being sent : " + dob);
		System.out.println(fis + " checked");
		clientEntry = new Client_Entry();

	}

	public static void clearData() {
		geburtsort.setText("");
		plz.setText("");
		ort.setText("");
		bundesland.setText("");
		street.setText("");
		telefon.setText("");
		vorName.setText("");
		name.setText("");
		geschlecht.setText("");

		txtVater.setText("");
		txtMutter.setText("");
		txtKonfession.setText("");
		txtNationalitt.setText("");
		txtMuttersprache.setText("");

		photo = null;
		canvas.redraw();

	}
}
