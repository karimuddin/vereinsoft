package com.chs.e4.rcp.todo.parts;

import java.util.List;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.chs.e4.rcp.todo.db.Client_Details;
import com.chs.e4.rcp.todo.db.Tagesdoku_Entry;

public class TagesdokuEntryPart extends Dialog {
	// private MySQLAccess msql;

	private Text txtBetreff;
	private String Client[];
	private Label lblErfasserName;
	private static String erfasser;
	private Text txtEintrag;
	private Combo comboKlient;
	private Combo comboKategorie;
	private org.eclipse.swt.widgets.List list;
	List<String> ID;

	@Inject
	public TagesdokuEntryPart(Shell shell) {
		super(shell);

		// TODO Auto-generated constructor stub
	}

	// private Text text;

	/*
	 * @PostConstruct public void createControls(Composite parent, EMenuService
	 * menuService) { parent.setLayout(new GridLayout(2, false));
	 * 
	 * Label lblNewLabel = new Label(parent, SWT.NONE);
	 * lblNewLabel.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
	 * false, 1, 1)); lblNewLabel.setText("New Label");
	 * 
	 * text = new Text(parent, SWT.BORDER); text.setLayoutData(new
	 * GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
	 * 
	 * }
	 */
	@Override
	protected Control createDialogArea(Composite parent) {

		Client_Details cDetails = new Client_Details();
		cDetails.get_ClientDetails();
		ID = cDetails.clientID;
		List<String> name = cDetails.clientName;
		List<String> family_name = cDetails.clientFamilyName;

		/*
		 * Login_Details ld = new Login_Details(); List<String> erfasser =
		 * ld.Username;
		 */

		try {
			Client = new String[ID.size()];
			for (int i = 0; i < ID.size(); i++) {

				Client[i] = name.get(i) + ", " + family_name.get(i);

				System.out.println("whats UP " + Client[i]);
			}

		} catch (

		Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(2, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);

		Label lblKind = new Label(container, SWT.NONE);
		GridData gd_lblKind = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_lblKind.widthHint = 71;
		lblKind.setLayoutData(gd_lblKind);
		lblKind.setText("Kind");

		comboKlient = new Combo(container, SWT.NONE);
		comboKlient.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		comboKlient.setItems(Client);
		comboKlient.select(0);

		Label lblBetreff = new Label(container, SWT.NONE);
		GridData gd_lblBetreff = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblBetreff.horizontalIndent = 1;
		lblBetreff.setLayoutData(gd_lblBetreff);
		lblBetreff.setText("Betreff");

		txtBetreff = new Text(container, SWT.BORDER);
		txtBetreff.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));

		Label lblKategorie = new Label(container, SWT.NONE);
		GridData gd_lblKategorie = new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1);
		gd_lblKategorie.widthHint = 72;
		lblKategorie.setLayoutData(gd_lblKategorie);
		lblKategorie.setText("Kategorie");

		comboKategorie = new Combo(container, SWT.NONE);
		comboKategorie.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1));
		String[] kategories = { "Familie", "Sozialverhalten", "Delinquenz", "Gesundheit/Sucht" };
		comboKategorie.setItems(kategories);
		comboKategorie.select(0);

		Label lblHilfeplan = new Label(container, SWT.NONE);
		lblHilfeplan.setText("Hilfeplan");

		list = new org.eclipse.swt.widgets.List(container, SWT.BORDER | SWT.MULTI | SWT.V_SCROLL);
		GridData gd_list = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_list.heightHint = 48;
		gd_list.widthHint = 156;
		list.setLayoutData(gd_list);
		list.add("Familie");
		list.add("Eltern");
		list.add("Geschwister");

		Label lblErfasser = new Label(container, SWT.NONE);
		GridData gd_lblErfasser = new GridData(SWT.LEFT, SWT.CENTER, false, false, 1, 1);
		gd_lblErfasser.widthHint = 40;
		lblErfasser.setLayoutData(gd_lblErfasser);
		lblErfasser.setText("Erfasser");

		lblErfasserName = new Label(container, SWT.NONE);
		lblErfasserName.setText(get_Erfasser() + "");

		Label lblEintrag = new Label(container, SWT.NONE);
		lblEintrag.setText("Eintrag");

		txtEintrag = new Text(container, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_txtEintrag = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_txtEintrag.heightHint = 31;
		txtEintrag.setLayoutData(gd_txtEintrag);
		txtEintrag.setTextLimit(450);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		/*
		 * Button btnDrcken = new Button(container, SWT.NONE);
		 * btnDrcken.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false,
		 * false, 1, 1)); btnDrcken.setText("Drücken");
		 * btnDrcken.addSelectionListener(new SelectionAdapter() {
		 * 
		 * @Override public void widgetSelected(SelectionEvent e) { final Image
		 * image = new Image(parent.getDisplay(), parent.getBounds()); ImageData
		 * imageData = image.getImageData();
		 * 
		 * PrintDialog printDialog = new PrintDialog(getShell(), SWT.NONE);
		 * printDialog.setText("Print"); PrinterData printerData =
		 * printDialog.open(); Printer p = new Printer(printerData); if
		 * (!(printerData == null)) { System.out.println(
		 * "printing started and Print Data" + printerData); Point screenDPI =
		 * parent.getDisplay().getDPI(); Point printerDPI = p.getDPI(); int
		 * scaleFactor = printerDPI.x / screenDPI.x; Rectangle trim =
		 * parent.getBounds();// p.computeTrim(0, 0, // 0, 0);
		 * 
		 * if (p.startJob("PrintJob")) { System.out.println("Print job started"
		 * ); if (p.startPage()) { System.out.println("Start Page"); GC gc = new
		 * GC(p);
		 * 
		 * Image printerImage = new Image(p, imageData);
		 * System.out.println("PrinterImage" + printerImage);
		 * gc.drawImage(printerImage, 0, 0);
		 * 
		 * printerImage.dispose(); gc.dispose();
		 * 
		 * p.endPage(); } }
		 * 
		 * } p.endJob(); p.dispose(); }
		 * 
		 * });
		 */
		/*
		 * Button button = new Button(container.getShell(), SWT.PUSH);
		 * button.setText("Print"); button.addListener(SWT.Selection, e -> {
		 * 
		 * Image image = new Image(container.getDisplay(),
		 * container.getBounds()); GC gc = new GC(image); boolean success =
		 * container.print(gc); PrintDialog printDialog = new
		 * PrintDialog(getShell(), SWT.NONE); printDialog.setText("Print");
		 * PrinterData printerData = printDialog.open(); Printer p = new
		 * Printer(printerData); p.startJob("PrintJob"); p.startPage(); GC
		 * gcPrinter = new GC(p); gcPrinter.drawImage(image,
		 * container.getBounds().x, container.getBounds().y); p.endPage();
		 * gcPrinter.dispose(); p.endJob(); p.dispose(); gc.dispose(); if
		 * (!success) { MessageBox messageBox = new MessageBox(getShell(),
		 * SWT.OK | SWT.PRIMARY_MODAL); messageBox.setMessage(
		 * "Sorry, taking a snapshot is not supported on your platform");
		 * messageBox.open(); } });
		 */

		return container;
	}

	// override method to use "Login" as label for the OK button
	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Submit", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected Point getInitialSize() {
		return new Point(767, 296);
	}

	@Override
	protected void okPressed() {
		// Defining Tagesdoku_entry class
		Tagesdoku_Entry tde = new Tagesdoku_Entry();

		// -- getting selected values for Hilfeplan
		String[] selected = new String[list.getSelectionCount()];
		String Hilfeplan = "";
		selected = list.getSelection();
		for (int i = 0; i < list.getSelectionCount(); i++) {
			if (i == 0) {
				Hilfeplan = selected[i];
			} else {
				Hilfeplan = Hilfeplan + ", " + selected[i];
			}
		}
		System.out.println(Hilfeplan);

		// passing values of the complete form

		tde.set_tagesdokuEntry(Integer.parseInt(ID.get(comboKlient.getSelectionIndex())), comboKlient.getText(),
				txtBetreff.getText() + "", comboKategorie.getText() + "", Hilfeplan, lblErfasserName.getText() + "",
				txtEintrag.getText() + "");
		super.okPressed();

	}

	public void set_Erfasser(String erf) {
		erfasser = erf;

	}

	public String get_Erfasser() {
		return erfasser;
	}

}
