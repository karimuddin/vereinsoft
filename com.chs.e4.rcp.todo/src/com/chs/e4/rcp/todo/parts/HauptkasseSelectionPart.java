package com.chs.e4.rcp.todo.parts;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

public class HauptkasseSelectionPart extends Dialog {
	private Button btnEingang;
	private Button btnAusgang;

	@Inject
	public HauptkasseSelectionPart(Shell shell) {
		super(shell);

		// TODO Auto-generated constructor stub
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(3, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblHauptkasse = new Label(container, SWT.NONE);
		lblHauptkasse.setFont(SWTResourceManager.getFont("Segoe UI", 13, SWT.BOLD));
		lblHauptkasse.setText("Hauptkasse");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnEingang = new Button(container, SWT.RADIO);
		btnEingang.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		btnEingang.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnEingang.setText("Eingang");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnAusgang = new Button(container, SWT.RADIO);
		btnAusgang.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnAusgang.setText("Ausgang");

		return container;

	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Submit", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected void okPressed() {
		if (btnEingang.getSelection()) {
			HauptkasseEingangPart hkEingang = new HauptkasseEingangPart(getShell());
			if (hkEingang.open() == Window.OK) {

			}

		} else if (btnAusgang.getSelection()) {
			HauptkasseAusgangPart hkAusgang = new HauptkasseAusgangPart(getShell());
			if (hkAusgang.open() == Window.OK) {

			}

		}

	}

}
