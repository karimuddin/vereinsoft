package com.chs.e4.rcp.todo.parts;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.wb.swt.SWTResourceManager;

public class KontoSelectionPart extends Dialog {
	private Button btnHauptkasse;
	private Button btnKinder;
	private Button btnLebensmittel;
	private Button btnBetreuung;
	private Button btnBudget;
	private Button btnGirokonto;

	@Inject
	public KontoSelectionPart(Shell shell) {
		super(shell);

		// TODO Auto-generated constructor stub
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(3, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnHauptkasse = new Button(container, SWT.RADIO);
		btnHauptkasse.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnHauptkasse.setText("Hauptkasse");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnGirokonto = new Button(container, SWT.RADIO);
		btnGirokonto.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnGirokonto.setText("Girokonto");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnKinder = new Button(container, SWT.RADIO);
		btnKinder.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnKinder.setText("Kinder");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnLebensmittel = new Button(container, SWT.RADIO);
		btnLebensmittel.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnLebensmittel.setText("Lebensmittel");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnBetreuung = new Button(container, SWT.RADIO);
		btnBetreuung.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnBetreuung.setText("Betreuung");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		btnBudget = new Button(container, SWT.RADIO);
		btnBudget.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		btnBudget.setText("Budget");

		return container;

	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Submit", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected void okPressed() {
		if (btnHauptkasse.getSelection()) {

			HauptkasseSelectionPart hkSelection = new HauptkasseSelectionPart(getShell());
			if (hkSelection.open() == Window.OK) {

			}

		} else if (btnGirokonto.getSelection()) {

			GirokontoPart gkPart = new GirokontoPart(getShell());
			if (gkPart.open() == Window.OK) {

			}

		} else if (btnKinder.getSelection()) {
			KasseEntryPart kePart = new KasseEntryPart(getShell());
			if (kePart.open() == Window.OK) {

			}

		} else if (btnLebensmittel.getSelection()) {
			LebensmittelPart lmPart = new LebensmittelPart(getShell());
			if (lmPart.open() == Window.OK) {

			}

		} else if (btnBetreuung.getSelection()) {
			BetreuungPart bePart = new BetreuungPart(getShell());
			if (bePart.open() == Window.OK) {

			}

		} else if (btnBudget.getSelection()) {
			BudgetPart bgPart = new BudgetPart(getShell());
			if (bgPart.open() == Window.OK) {

			}

		}

	}

}
