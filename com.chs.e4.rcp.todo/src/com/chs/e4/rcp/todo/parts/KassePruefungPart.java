package com.chs.e4.rcp.todo.parts;

import java.text.NumberFormat;
import java.util.List;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.wb.swt.SWTResourceManager;

import com.chs.e4.rcp.todo.db.Pruefung_Get;
import com.chs.e4.rcp.todo.db.Pruefung_Set;

public class KassePruefungPart extends Dialog {
	Spinner spnr1Cent;
	Spinner spnr2Cent;
	Spinner spnr5Cent;
	Spinner spnr10Cent;
	Spinner spnr20Cent;
	Spinner spnr50Cent;
	Spinner spnr1Eur;
	Spinner spnr2Eur;
	Spinner spnr5Eur;
	Spinner spnr10Eur;
	Spinner spnr20Eur;
	Spinner spnr50Eur;
	Spinner spnr100Eur;
	Spinner spnr200Eur;
	Spinner spnrSchickheft;
	Label lblNewLabel_2;
	List<Float> pgesamtbeitrag;

	@Inject
	public KassePruefungPart(Shell shell) {
		super(shell);

		// TODO Auto-generated constructor stub
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(6, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);

		Pruefung_Get pGet = new Pruefung_Get();
		pGet.get_KassePruefung();
		List<Integer> einCent = pGet.einCent;
		List<Integer> zweiCent = pGet.zweiCent;
		List<Integer> funfCent = pGet.funfCent;
		List<Integer> zehnCent = pGet.zehnCent;
		List<Integer> zwenzigCent = pGet.zwenzigCent;
		List<Integer> funfzigCent = pGet.funfzigCent;
		List<Integer> einEuro = pGet.einEuro;
		List<Integer> zweiEuro = pGet.zweiEuro;
		List<Integer> funfEuro = pGet.funfEuro;
		List<Integer> zehnEuro = pGet.zehnEuro;
		List<Integer> zwenzigEuro = pGet.zwenzigEuro;
		List<Integer> funfzigEuro = pGet.funfzigEuro;
		List<Integer> hundertEuro = pGet.hundertEuro;
		List<Integer> zweihundertEuro = pGet.zweihundertEuro;
		List<Integer> schickheft = pGet.schickheft;
		pgesamtbeitrag = pGet.gesamtbeitrag;

		spnr1Cent = new Spinner(container, SWT.BORDER);
		spnr1Cent.setMaximum(100000000);
		spnr1Cent.setSelection(einCent.get(0));
		spnr1Cent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label lblCent = new Label(container, SWT.NONE);
		lblCent.setText(" 1 Cent ");

		spnr1Eur = new Spinner(container, SWT.BORDER);
		spnr1Eur.setMaximum(100000000);
		spnr1Eur.setSelection(einEuro.get(0));
		spnr1Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label = new Label(container, SWT.NONE);
		label.setText(" 1 €");

		spnr100Eur = new Spinner(container, SWT.BORDER);
		spnr100Eur.setMaximum(100000000);
		spnr100Eur.setSelection(hundertEuro.get(0));
		spnr100Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label_6 = new Label(container, SWT.NONE);
		label_6.setText(" 100 € ");

		spnr2Cent = new Spinner(container, SWT.BORDER);
		spnr2Cent.setMaximum(100000000);
		spnr2Cent.setSelection(zweiCent.get(0));
		spnr2Cent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label lblCent_1 = new Label(container, SWT.NONE);
		lblCent_1.setText(" 2 Cent ");

		spnr2Eur = new Spinner(container, SWT.BORDER);
		spnr2Eur.setMaximum(100000000);
		spnr2Eur.setSelection(zweiEuro.get(0));
		spnr2Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label_1 = new Label(container, SWT.NONE);
		label_1.setText(" 2 € ");

		spnr200Eur = new Spinner(container, SWT.BORDER);
		spnr200Eur.setMaximum(100000000);
		spnr200Eur.setSelection(zweihundertEuro.get(0));
		spnr200Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label_7 = new Label(container, SWT.NONE);
		label_7.setText(" 200 € ");

		spnr5Cent = new Spinner(container, SWT.BORDER);
		spnr5Cent.setMaximum(100000000);
		spnr5Cent.setSelection(funfCent.get(0));
		spnr5Cent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label lblCent_2 = new Label(container, SWT.NONE);
		lblCent_2.setText(" 5 Cent ");

		spnr5Eur = new Spinner(container, SWT.BORDER);
		spnr5Eur.setMaximum(100000000);
		spnr5Eur.setSelection(funfEuro.get(0));
		spnr5Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label_2 = new Label(container, SWT.NONE);
		label_2.setText(" 5 € ");

		spnrSchickheft = new Spinner(container, SWT.BORDER);
		spnrSchickheft.setDigits(2);
		spnrSchickheft.setMaximum(100000000);
		spnrSchickheft.setSelection(schickheft.get(0));
		spnrSchickheft.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label lblNewLabel = new Label(container, SWT.NONE);
		lblNewLabel.setText(" Schickheft ");

		spnr10Cent = new Spinner(container, SWT.BORDER);
		spnr10Cent.setMaximum(100000000);
		spnr10Cent.setSelection(zehnCent.get(0));
		spnr10Cent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label lblCent_3 = new Label(container, SWT.NONE);
		lblCent_3.setText(" 10 Cent ");

		spnr10Eur = new Spinner(container, SWT.BORDER);
		spnr10Eur.setMaximum(100000000);
		spnr10Eur.setSelection(zehnEuro.get(0));
		spnr10Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label_3 = new Label(container, SWT.NONE);
		label_3.setText(" 10 € ");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		spnr20Cent = new Spinner(container, SWT.BORDER);
		spnr20Cent.setMaximum(100000000);
		spnr20Cent.setSelection(zwenzigCent.get(0));
		spnr20Cent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label lblCent_4 = new Label(container, SWT.NONE);
		lblCent_4.setText(" 20 Cent ");

		spnr20Eur = new Spinner(container, SWT.BORDER);
		spnr20Eur.setMaximum(100000000);
		spnr20Eur.setSelection(zwenzigEuro.get(0));
		spnr20Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label_4 = new Label(container, SWT.NONE);
		label_4.setText(" 20 € ");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		spnr50Cent = new Spinner(container, SWT.BORDER);
		spnr50Cent.setMaximum(100000000);
		spnr50Cent.setSelection(funfzigCent.get(0));
		spnr50Cent.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label lblCent_5 = new Label(container, SWT.NONE);
		lblCent_5.setText(" 50 Cent ");

		spnr50Eur = new Spinner(container, SWT.BORDER);
		spnr50Eur.setMaximum(100000000);
		spnr50Eur.setSelection(funfzigEuro.get(0));
		spnr50Eur.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});

		Label label_5 = new Label(container, SWT.NONE);
		label_5.setText(" 50 € ");

		Label lblNewLabel_1 = new Label(container, SWT.NONE);
		lblNewLabel_1.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblGesamtAnzahl = new Label(container, SWT.NONE);
		lblGesamtAnzahl.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.BOLD));
		lblGesamtAnzahl.setText("Gesamt Anzahl");

		lblNewLabel_2 = new Label(container, SWT.NONE);
		lblNewLabel_2.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		lblNewLabel_2.setText("" + NumberFormat.getInstance().format(calc_GesamtBeitrag()));

		return container;
	}

	public float calc_GesamtBeitrag() {
		float Cents = spnr1Cent.getSelection() + 2 * spnr2Cent.getSelection() + 5 * spnr5Cent.getSelection()
				+ 10 * spnr10Cent.getSelection() + 20 * spnr20Cent.getSelection() + 50 * spnr50Cent.getSelection()
				+ 1 * spnrSchickheft.getSelection();
		int Euros = spnr1Eur.getSelection() + 2 * spnr2Eur.getSelection() + 5 * spnr5Eur.getSelection()
				+ 10 * spnr10Eur.getSelection() + 20 * spnr20Eur.getSelection() + 50 * spnr50Eur.getSelection()
				+ 100 * spnr100Eur.getSelection() + 200 * spnr200Eur.getSelection();

		float gesamtbeitrag = Euros + (Cents / 100);

		return gesamtbeitrag;

	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Submit", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected void okPressed() {

		float gesamtbeitrag = calc_GesamtBeitrag();
		float differenz = gesamtbeitrag - pgesamtbeitrag.get(0);
		Pruefung_Set pSet = new Pruefung_Set();

		// passing values of the complete form

		pSet.set_KassePruefung(spnr1Cent.getSelection(), spnr2Cent.getSelection(), spnr5Cent.getSelection(),
				spnr10Cent.getSelection(), spnr20Cent.getSelection(), spnr50Cent.getSelection(),
				spnr1Eur.getSelection(), spnr2Eur.getSelection(), spnr5Eur.getSelection(), spnr10Eur.getSelection(),
				spnr20Eur.getSelection(), spnr50Eur.getSelection(), spnr100Eur.getSelection(),
				spnr200Eur.getSelection(), spnrSchickheft.getSelection(), gesamtbeitrag);

		System.out.println("Difference is " + differenz + ":" + gesamtbeitrag + ":" + (pgesamtbeitrag.get(0)));

		/*
		 * kPart.lblBargeldbetrag.setText(NumberFormat.getInstance().format(
		 * gesamtbeitrag) + "");
		 */

		super.okPressed();

	}

}
