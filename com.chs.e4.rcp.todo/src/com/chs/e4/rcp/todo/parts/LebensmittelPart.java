package com.chs.e4.rcp.todo.parts;

import java.sql.Date;

import javax.inject.Inject;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;

import com.chs.e4.rcp.todo.db.Kasse_Entry;

public class LebensmittelPart extends Dialog {
	private static Date date;
	private Label lblErfasserName;
	private Combo combo;
	private Text kommentare;
	private Spinner spnrBetrag;
	private static String erfasser;

	@Inject
	public LebensmittelPart(Shell shell) {
		super(shell);

		// TODO Auto-generated constructor stub
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite container = (Composite) super.createDialogArea(parent);
		GridLayout layout = new GridLayout(4, false);
		layout.marginRight = 5;
		layout.marginLeft = 10;
		container.setLayout(layout);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblHauptkasseeingang = new Label(container, SWT.NONE);
		lblHauptkasseeingang.setFont(SWTResourceManager.getFont("Segoe UI Light", 11, SWT.BOLD));
		lblHauptkasseeingang.setText("Lebensmittel");
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblNewLabel_1 = new Label(container, SWT.NONE);
		lblNewLabel_1.setText("Datum");

		DateTime dateTime = new DateTime(container, SWT.BORDER);
		GridData gd_dateTime = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_dateTime.widthHint = 91;
		dateTime.setLayoutData(gd_dateTime);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblBetrag = new Label(container, SWT.NONE);
		lblBetrag.setText("Betrag");
		spnrBetrag = new Spinner(container, SWT.BORDER);
		GridData gd_spnrBetrag = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_spnrBetrag.widthHint = 67;
		spnrBetrag.setLayoutData(gd_spnrBetrag);
		spnrBetrag.setDigits(2);
		spnrBetrag.setMaximum(100000000);
		// here operation from Database
		// spnrBetrag.setSelection(funfhundertEuro.get(0));
		spnrBetrag.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				// lblNewLabel_2.setText("" +
				// NumberFormat.getInstance().format(calc_GesamtBeitrag()));
			}
		});
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblGegenkonto = new Label(container, SWT.NONE);
		lblGegenkonto.setText("Gegenkonto");

		combo = new Combo(container, SWT.NONE);
		GridData gd_combo = new GridData(SWT.FILL, SWT.CENTER, true, false, 1, 1);
		gd_combo.widthHint = 87;
		combo.setLayoutData(gd_combo);
		String[] kategories = { "Hauptkasse", "Girokonto" };
		combo.setItems(kategories);
		combo.select(0);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblKommentare = new Label(container, SWT.NONE);
		lblKommentare.setText("Kommentare");

		kommentare = new Text(container, SWT.MULTI | SWT.BORDER | SWT.WRAP | SWT.V_SCROLL);
		GridData gd_list = new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1);
		gd_list.heightHint = 36;
		kommentare.setLayoutData(gd_list);
		kommentare.setTextLimit(200);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Label lblErfasser = new Label(container, SWT.NONE);
		lblErfasser.setText("Erfasser");

		lblErfasserName = new Label(container, SWT.NONE);
		lblErfasserName.setText(get_Erfasser() + "");
		date = new Date(dateTime.getYear() - 1900, dateTime.getMonth(), dateTime.getDay());
		dateTime.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				System.out.println("calendar date changed");
				date = new Date(dateTime.getYear() - 1900, dateTime.getMonth(), dateTime.getDay());
				System.out.print("Date of Birth is " + date);
			}

		});

		return container;
	}

	@Override
	protected void createButtonsForButtonBar(Composite parent) {
		createButton(parent, IDialogConstants.OK_ID, "Submit", true);
		createButton(parent, IDialogConstants.CANCEL_ID, IDialogConstants.CANCEL_LABEL, false);
	}

	@Override
	protected void okPressed() {
		// Converting amount to float
		float betrag = spnrBetrag.getSelection() / (float) 100;

		Kasse_Entry ke = new Kasse_Entry();
		String gegenkonto = combo.getItem(combo.getSelectionIndex());

		if (gegenkonto.equals("Hauptkasse")) {
			ke.set_LebensmittelEntry(betrag);

			ke.set_HauptkasseEntry(-betrag);

			ke.set_kasseEntry(0, "Lebensmittel", gegenkonto, -betrag, kommentare.getText() + "",
					lblErfasserName.getText() + "");

		} else if (gegenkonto.equals("Girokonto")) {
			ke.set_LebensmittelEntry(betrag);

			ke.set_GirokontoEntry(-betrag);

			ke.set_kasseEntry(0, "Lebensmittel", gegenkonto, -betrag, kommentare.getText() + "",
					lblErfasserName.getText() + "");
		}
		super.okPressed();
	}

	public void set_Erfasser(String erf) {
		erfasser = erf;

	}

	public String get_Erfasser() {
		return erfasser;
	}
}
