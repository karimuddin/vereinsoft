package com.chs.e4.rcp.todo.parts;

import java.text.NumberFormat;
import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.wb.swt.SWTResourceManager;

import com.chs.e4.rcp.todo.db.Kasse_Details;
import com.chs.e4.rcp.todo.db.KassenKonten_Details;
import com.chs.e4.rcp.todo.db.Pruefung_Get;

public class KassePart {
	private Table table;
	// private Float gesamtbetrag;
	public Label lblKassebetrag;
	public Label lblBargeldbetrag;
	public Label lblGirokontobetrag;
	public Label lblLebensmittelbetrag;
	public Label lblBudgetbetrag;
	public Label lblBetreuungbetrag;
	public Composite composite;

	private Kasse_Details kd;
	public KassenKonten_Details kkd;
	public Pruefung_Get pfGet;
	private List<String> idKasse;
	private List<String> idClient;
	private List<String> clientName;
	private List<String> account;
	private List<Float> amount;
	private List<String> comment;
	private List<String> date;
	private List<String> erfasser;

	private List<Float> lebensmittel;
	private List<Float> betreuung;
	private List<Float> budget;
	private List<Float> girokonto;
	private List<Float> kasse;
	private List<Float> kassePruefung;

	@PostConstruct
	public void createControls(Composite parent) {

		// getting Kasse details from class Kasse_Details
		kd = new Kasse_Details();
		kd.get_KasseDetails();

		kkd = new KassenKonten_Details();
		kkd.get_KassenKontenDetails();

		pfGet = new Pruefung_Get();
		pfGet.get_KassePruefung();

		idKasse = kd.idKasse;
		idClient = kd.idClient;
		clientName = kd.clientName;
		account = kd.account;
		amount = kd.amount;
		comment = kd.comment;
		date = kd.date;
		erfasser = kd.erfasser;

		lebensmittel = kkd.lebensmittel;
		betreuung = kkd.betreuung;
		budget = kkd.budget;
		girokonto = kkd.girokonto;
		kasse = kkd.kasse;

		kassePruefung = pfGet.gesamtbeitrag;

		// Setting column names for table
		String[] titles = { "Datum", "Name", "Konto", "Saldo", "Kommentare", "Erfasser" };
		parent.setLayout(null);

		// new JFace table
		TableViewer tableviewer = new TableViewer(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		table = tableviewer.getTable();
		table.setBounds(0, 0, 755, 449);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		Composite composite = new Composite(parent, SWT.NONE);
		composite.setBounds(761, 0, 248, 449);
		composite.setLayout(new GridLayout(5, false));
		composite.layout();
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Label lblHauptkasse = new Label(composite, SWT.NONE);
		lblHauptkasse.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.BOLD));
		lblHauptkasse.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER, false, false, 1, 1));
		lblHauptkasse.setText("Hauptkasse");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Label lblKasse = new Label(composite, SWT.NONE);
		lblKasse.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblKasse.setText("+ Kasse");
		new Label(composite, SWT.NONE);

		lblKassebetrag = new Label(composite, SWT.NONE);
		lblKassebetrag.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblKassebetrag.setText(NumberFormat.getInstance().format(kasse.get(0)) + "");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Label lblBargeld = new Label(composite, SWT.NONE);
		lblBargeld.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblBargeld.setText("+ Bargeld");
		new Label(composite, SWT.NONE);

		lblBargeldbetrag = new Label(composite, SWT.NONE);
		lblBargeldbetrag.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblBargeldbetrag.setText(NumberFormat.getInstance().format(kassePruefung.get(0)) + "");

		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Label lblGirokonto = new Label(composite, SWT.NONE);
		lblGirokonto.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblGirokonto.setText("+ Girokonto");
		new Label(composite, SWT.NONE);

		lblGirokontobetrag = new Label(composite, SWT.NONE);
		lblGirokontobetrag.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblGirokontobetrag.setText(NumberFormat.getInstance().format(girokonto.get(0)) + "");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Label lblLebenmittel = new Label(composite, SWT.NONE);
		lblLebenmittel.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblLebenmittel.setText("- Lebensmittel");
		new Label(composite, SWT.NONE);

		lblLebensmittelbetrag = new Label(composite, SWT.NONE);
		lblLebensmittelbetrag.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblLebensmittelbetrag.setText(NumberFormat.getInstance().format(lebensmittel.get(0)) + "");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Label lblbudget = new Label(composite, SWT.NONE);
		lblbudget.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblbudget.setText("- Budget");
		new Label(composite, SWT.NONE);

		lblBudgetbetrag = new Label(composite, SWT.NONE);
		lblBudgetbetrag.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblBudgetbetrag.setText(NumberFormat.getInstance().format(budget.get(0)) + "");
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Label lblBetreuung = new Label(composite, SWT.NONE);
		lblBetreuung.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblBetreuung.setText("- Betreuung");
		new Label(composite, SWT.NONE);

		lblBetreuungbetrag = new Label(composite, SWT.NONE);
		lblBetreuungbetrag.setFont(SWTResourceManager.getFont("Segoe UI", 12, SWT.NORMAL));
		lblBetreuungbetrag.setText(NumberFormat.getInstance().format(betreuung.get(0)) + "");

		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		Button btnAktualisieren = new Button(composite, SWT.PUSH);
		btnAktualisieren.setText("Aktualisieren");
		btnAktualisieren.addListener(SWT.MouseDown, new Listener() {
			public void handleEvent(Event e) {
				kd = new Kasse_Details();
				kd.get_KasseDetails();

				kkd = new KassenKonten_Details();
				kkd.get_KassenKontenDetails();

				pfGet = new Pruefung_Get();
				pfGet.get_KassePruefung();

				idKasse = kd.idKasse;
				idClient = kd.idClient;
				clientName = kd.clientName;
				account = kd.account;
				amount = kd.amount;
				comment = kd.comment;
				date = kd.date;
				erfasser = kd.erfasser;

				lebensmittel = kkd.lebensmittel;
				betreuung = kkd.betreuung;
				budget = kkd.budget;
				girokonto = kkd.girokonto;
				kasse = kkd.kasse;

				kassePruefung = pfGet.gesamtbeitrag;

				lblKassebetrag.setText(NumberFormat.getInstance().format(kasse.get(0)) + "");
				lblBargeldbetrag.setText(NumberFormat.getInstance().format(kassePruefung.get(0)) + "");
				lblGirokontobetrag.setText(NumberFormat.getInstance().format(girokonto.get(0)) + "");
				lblLebensmittelbetrag.setText(NumberFormat.getInstance().format(lebensmittel.get(0)) + "");
				lblBudgetbetrag.setText(NumberFormat.getInstance().format(budget.get(0)) + "");
				lblBetreuungbetrag.setText(NumberFormat.getInstance().format(betreuung.get(0)) + "");

				composite.redraw();
				composite.layout();
				parent.redraw();
				parent.layout();

			}
		});
		new Label(composite, SWT.NONE);
		new Label(composite, SWT.NONE);

		/*
		 * Label lblGesamtanzahl = new Label(composite, SWT.NONE);
		 * lblGesamtanzahl.setFont(SWTResourceManager.getFont("Segoe UI", 12,
		 * SWT.BOLD)); lblGesamtanzahl.setLayoutData(new GridData(SWT.CENTER,
		 * SWT.CENTER, false, false, 1, 1));
		 * lblGesamtanzahl.setText("Gesamtanzahl"); new Label(composite,
		 * SWT.NONE);
		 * 
		 * Label lblGesamtbetrag = new Label(composite, SWT.NONE);
		 * lblGesamtbetrag.setLayoutData(new GridData(SWT.CENTER, SWT.CENTER,
		 * false, false, 1, 1));
		 * lblGesamtbetrag.setFont(SWTResourceManager.getFont("Segoe UI", 12,
		 * SWT.NORMAL)); gesamtbetrag = (kasse.get(0) + girokonto.get(0)) -
		 * (lebensmittel.get(0) + budget.get(0) + betreuung.get(0));
		 * lblGesamtbetrag.setText(NumberFormat.getInstance().format(
		 * gesamtbetrag) + "");
		 */
		// populating with column names
		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(table, SWT.NONE);
			column.setText(titles[i]);

		}

		// Populating each row with data
		try {
			for (int i = 0; i < idKasse.size(); i++) {

				TableItem item = new TableItem(table, SWT.NONE);
				item.setText(0, date.get(i));
				item.setText(1, clientName.get(i));
				item.setText(2, account.get(i));
				item.setText(3, amount.get(i) + "");
				item.setText(4, comment.get(i));
				item.setText(5, erfasser.get(i));

				/*
				 * System.out.println( ID.get(i) + " " + name.get(i) + " " +
				 * family_name.get(i) + " " + gender.get(i) + " " + dob.get(i));
				 */

			}
		} catch (

		Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < titles.length; i++) {
			table.getColumn(i).pack();
		}
	}
}
