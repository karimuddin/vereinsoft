package com.chs.e4.rcp.todo.parts;

import java.util.List;

import javax.annotation.PostConstruct;

import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.browser.Browser;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.Text;

import com.chs.e4.rcp.todo.db.Client_Details;

public class TodoDetailsPart {
	private Table table;
	private Text text;
	private Browser browser;

	@PostConstruct
	public void createControls(Composite parent, EMenuService menuService) {
		/*
		 * System.out.println(this.getClass().getSimpleName() +
		 * " @PostConstruct method called."); }
		 */
		// getting client details from class Client_Details
		Client_Details cd = new Client_Details();
		cd.get_ClientDetails();
		cd.get_ClientAddress();
		List<String> ID = cd.clientID;
		List<String> name = cd.clientName;
		List<String> family_name = cd.clientFamilyName;
		List<String> gender = cd.clientGender;
		List<String> dob = cd.DOB;
		List<String> street = cd.clientStreet;
		List<String> house = cd.clientHouseNum;
		List<String> plz = cd.clientPostalCode;
		List<String> city = cd.clientCity;
		List<String> state = cd.clientState;
		List<String> country = cd.clientCountry;
		List<String> telephone = cd.clientTelephone;
		List<String> clientAddressId = cd.clientAddressID;

		// Setting column names for table
		String[] titles = { "Klient ID", "Name", "Nachname", "Geschlecht", "Geburtsdatum", "Straße", "PLZ", "Ort",
				"Bundesland", "Telephone" };

		// Tab1 for Details Composite
		/*
		 * final CTabFolder tabFolder1 = new CTabFolder(parent, SWT.BORDER);
		 * CTabItem tabItem1 = new CTabItem(tabFolder1, SWT.NULL);
		 * tabItem1.setText("Table xyz");
		 */

		// new JFace table
		TableViewer tableviewer = new TableViewer(parent,
				SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL | SWT.FULL_SELECTION | SWT.BORDER);
		table = tableviewer.getTable();
		table.setSize(448, 258);
		table.setLinesVisible(true);
		table.setHeaderVisible(true);

		// populating with column names
		for (int i = 0; i < titles.length; i++) {
			TableColumn column = new TableColumn(table, SWT.NONE);
			column.setText(titles[i]);
		}

		// Populating each row with data
		try {
			for (int i = 0; i < ID.size(); i++) {

				if (ID.get(i).equals(clientAddressId.get(i))) {
					TableItem item = new TableItem(table, SWT.NONE);
					item.setText(0, ID.get(i));
					item.setText(1, name.get(i));
					item.setText(2, family_name.get(i));
					item.setText(3, gender.get(i));
					item.setText(4, dob.get(i));
					item.setText(5, street.get(i));
					item.setText(6, house.get(i));
					item.setText(7, city.get(i));
					item.setText(8, state.get(i));
					item.setText(9, telephone.get(i));
					/*
					 * System.out.println( ID.get(i) + " " + name.get(i) + " " +
					 * family_name.get(i) + " " + gender.get(i) + " " +
					 * dob.get(i));
					 */
				} else {
					System.out.println(
							"Clients ID doesn't match with Address ID in " + ID.get(i) + " " + clientAddressId.get(i));
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < titles.length; i++) {
			table.getColumn(i).pack();
		}
		// tabFolder1.setSize(400, 200);

		// -------------------------Tab2 part starts
		// here------------------------------------------------

		// Tab1 for Details Composite
		/*
		 * final CTabFolder tabFolder2 = new CTabFolder(parent, SWT.BORDER);
		 * 
		 * CTabItem tabItem2 = new CTabItem(tabFolder1, SWT.NULL);
		 * tabItem2.setText("Map");
		 * 
		 * tabFolder2.setLayout(new GridLayout(2, false));
		 * 
		 * text = new Text(tabFolder2, SWT.BORDER); text.setMessage("Enter City"
		 * ); text.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, true, false,
		 * 1, 1));
		 * 
		 * Button button = new Button(tabFolder2, SWT.PUSH);
		 * button.setText("Search"); button.addSelectionListener(new
		 * SelectionAdapter() {
		 * 
		 * @Override public void widgetSelected(SelectionEvent e) { String city
		 * = text.getText(); if (city.isEmpty()) { return; } try { // not
		 * supported at the moment by Google //
		 * browser.setUrl("http://maps.google.com/maps?q=" // +
		 * URLEncoder.encode(city, "UTF-8") // + "&output=embed");
		 * browser.setUrl( "https://www.google.com/maps/place/" +
		 * URLEncoder.encode(city, "UTF-8") + "/&output=embed");
		 * 
		 * } catch (UnsupportedEncodingException e1) { e1.printStackTrace(); } }
		 * });
		 * 
		 * browser = new Browser(parent, SWT.NONE); browser.setLayoutData(new
		 * GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
		 * 
		 * // tabFolder1.setSize(400, 200);
		 */ }

	private void If(boolean b) {
		// TODO Auto-generated method stub

	}
}
