package com.chs.e4.rcp.todo;

import org.eclipse.e4.core.contexts.ContextInjectionFactory;
import org.eclipse.e4.core.contexts.IEclipseContext;
import org.eclipse.e4.ui.workbench.lifecycle.PostContextCreate;
import org.eclipse.e4.ui.workbench.lifecycle.PreSave;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessAdditions;
import org.eclipse.e4.ui.workbench.lifecycle.ProcessRemovals;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Shell;

/**
 * This is a stub implementation containing e4 LifeCycle annotated methods.
 * <br />
 * There is a corresponding entry in <em>plugin.xml</em> (under the
 * <em>org.eclipse.core.runtime.products' extension point</em>) that references
 * this class.
 **/
@SuppressWarnings("restriction")
public class E4LifeCycle {

	@PostContextCreate
	void postContextCreate(IEclipseContext workbenchContext) {

	}

	@PreSave
	void preSave(IEclipseContext workbenchContext) {
	}

	@ProcessAdditions
	void processAdditions(IEclipseContext workbenchContext) {
		final Shell shell = new Shell(SWT.TOOL | SWT.NO_TRIM);
		LoginDialog dialog = new LoginDialog(shell);
		ContextInjectionFactory.inject(dialog, workbenchContext);
		if (dialog.open() != Window.OK) {
			// close application
			System.exit(-1);
		}

		// continue to the initial window
	}

	@ProcessRemovals
	void processRemovals(IEclipseContext workbenchContext) {
	}
}
