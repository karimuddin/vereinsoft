package com.chs.e4.rcp.todo;

import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.MApplication;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.chs.e4.rcp.todo.db.Login_Details;
import com.chs.e4.rcp.todo.parts.BetreuungPart;
import com.chs.e4.rcp.todo.parts.BudgetPart;
import com.chs.e4.rcp.todo.parts.GirokontoPart;
import com.chs.e4.rcp.todo.parts.HauptkasseAusgangPart;
import com.chs.e4.rcp.todo.parts.HauptkasseEingangPart;
import com.chs.e4.rcp.todo.parts.KasseEntryPart;
import com.chs.e4.rcp.todo.parts.LebensmittelPart;
import com.chs.e4.rcp.todo.parts.TagesdokuEntryPart;

public class LoginDialog extends TitleAreaDialog {
	// Widgets
	private Text textUsername;
	private Text textPassword;

	@Inject
	MApplication application;

	public LoginDialog(Shell parentShell) {
		super(parentShell);
	}

	@Override
	protected Control createContents(Composite parent) {
		Control contents = super.createContents(parent);
		setTitle("Anmeldung");
		setMessage("Bitte geben Sie Ihre Zugangsdaten");
		return contents;
	}

	@Override
	protected Control createDialogArea(Composite parent) {
		Composite area = (Composite) super.createDialogArea(parent);

		System.out.println(application);

		Composite container = new Composite(area, SWT.NULL);
		container.setLayout(new GridLayout(2, false));
		container.setLayoutData(new GridData(GridData.FILL_BOTH));

		new Label(container, SWT.NULL).setText("Benutzername");
		textUsername = new Text(container, SWT.BORDER);
		textUsername.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		new Label(container, SWT.NULL).setText("Kennwort");
		textPassword = new Text(container, SWT.PASSWORD | SWT.BORDER);
		textPassword.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);
		new Label(container, SWT.NONE);

		Link link = new Link(container, SWT.BORDER);
		link.setLayoutData(new GridData(SWT.RIGHT, SWT.CENTER, false, false, 1, 1));
		link.setText("Bitte <A>registrieren</A> Sie sich als Mitarbeiter, wenn Sie nicht bereits Login besitzen");
		link.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(final SelectionEvent e) {
				final Shell shell = new Shell(SWT.TOOL | SWT.NO_TRIM);
				SignupDialog dialog = new SignupDialog(shell);
				if (dialog.open() != Window.OK) {
					// close application
					System.exit(-1);
				}
			}
		});
		// link.setSize(140, 40);

		return area;
	}

	@Override
	protected Point getInitialSize() {
		return new Point(554, 325);
	}

	@Override
	protected void okPressed() {
		Login_Details ld = new Login_Details();
		String Username = textUsername.getText();
		int result = ld.get_LoginDetails(Username, textPassword.getText());

		if (result > 0) {
			TagesdokuEntryPart tdp = new TagesdokuEntryPart(getShell());
			tdp.set_Erfasser(Username);

			KasseEntryPart kep = new KasseEntryPart(getShell());
			kep.set_Erfasser(Username);

			HauptkasseEingangPart hep = new HauptkasseEingangPart(getShell());
			hep.set_Erfasser(Username);

			HauptkasseAusgangPart hap = new HauptkasseAusgangPart(getShell());
			hap.set_Erfasser(Username);

			GirokontoPart gkp = new GirokontoPart(getShell());
			gkp.set_Erfasser(Username);

			LebensmittelPart lbp = new LebensmittelPart(getShell());
			lbp.set_Erfasser(Username);

			BetreuungPart btp = new BetreuungPart(getShell());
			btp.set_Erfasser(Username);

			BudgetPart bgp = new BudgetPart(getShell());
			bgp.set_Erfasser(Username);

			super.okPressed();
		} else {
			MessageDialog.openError(new Shell(), "Falsche Anmeldung",
					"Bitte geben sie vollstandinge und richtige Anmeldeinformationen");
		}

	}

	@Override
	protected void cancelPressed() {
		System.exit(-1);
	}

}
